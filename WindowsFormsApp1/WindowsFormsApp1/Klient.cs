﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Klient : Osoba
    {
        private List<int> wypozyczone=null;

        public List<int> Wypozyczone { get => wypozyczone; set => wypozyczone = value; }

        public Klient(string imie, string nazwisko, string numerISeriaDowodu, string login, string haslo, short wiek, int id, List<int> wypozyczone) : base(imie, nazwisko, numerISeriaDowodu, login, haslo, wiek, id)
        {
            this.wypozyczone = wypozyczone;
            wypozyczone = new List<int>();
        }

        public void wypozycz(int id)
        {
            wypozyczone.Add(id);
        }
        public double oddaj(int id,Garaz g)
        {
            for(int i=0;i<wypozyczone.Count;i++)
            {
                if (wypozyczone[i] == id)
                {
                    wypozyczone.Remove(wypozyczone[i]);
                    
                    break;
                }
            }
            foreach(Osobowy po in g.PojazdyOsobowe)
            {
                if(id==po.Id)
                {
                    TimeSpan ts = DateTime.Now - po.Datawy;
                    return 10 + ((ts.TotalDays) * po.Cena);
                }
            }
            foreach(Ciezarowy pc in g.PojazdyCiezarowe)
            {
                if(id==pc.Id)
                {
                    TimeSpan ts = DateTime.Now - pc.Datawy;
                    return 80 + ((ts.TotalDays) * pc.Cena);
                }
            }
            return 0;
        }
    }
}
