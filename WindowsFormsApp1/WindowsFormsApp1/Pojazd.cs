﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Pojazd
    {
        private string vIN;
        string marka;
        string model;
        string kolor;
        int konieMechaniczne;
        bool wypozyczony;
        int id;
        DateTime datawy;
        double cena;

        public Pojazd(string kolor, string vIN, string marka, string model, int konieMechaniczne, bool wypozyczony, int id, DateTime datawy,double cena)
        {
            this.kolor = kolor;
            this.vIN = vIN;
            this.marka = marka;
            this.model = model;
            this.konieMechaniczne = konieMechaniczne;
            this.wypozyczony = wypozyczony;
            this.id = id;
            this.datawy = datawy;
            this.Cena = cena;
        }

        public string VIN { get => vIN; set => vIN = value; }
        public string Marka { get => marka; set => marka = value; }
        public string Model { get => model; set => model = value; }
        public int KonieMechaniczne { get => konieMechaniczne; set => konieMechaniczne = value; }
        public bool Wypozyczony { get => wypozyczony; set => wypozyczony = value; }
        public int Id { get => id; set => id = value; }
        public DateTime Datawy { get => datawy; set => datawy = value; }
        public string Kolor { get => kolor; set => kolor = value; }
        public double Cena { get => cena; set => cena = value; }
    }
}
