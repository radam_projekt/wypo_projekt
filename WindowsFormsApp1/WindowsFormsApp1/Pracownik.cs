﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Pracownik : Osoba
    {
        public Pracownik(string imie, string nazwisko, string numerISeriaDowodu, string login, string haslo, short wiek, int id)
            : base(imie, nazwisko, numerISeriaDowodu, login, haslo, wiek, id)
        {
        }

        public void usunUzytkownika(long id,List<Klient> lista)
        {
            for(int i=0;i<lista.Count;i++)
            {
                if(lista[i].Id==id)
                {
                    lista.Remove(lista[i]);
                }
            }
        }
        public bool usunSamochod(long id,Garaz g)
        {
            for(int i=0;i<g.PojazdyCiezarowe.Count+g.PojazdyOsobowe.Count;i++)
            {
                try
                {
                    if (g.PojazdyOsobowe[i].Id == id)
                    {
                        g.PojazdyOsobowe.Remove(g.PojazdyOsobowe[i]);
                        System.IO.File.Delete(g.Path + "/car_000" + id.ToString() + ".car");
                        return true;
                    }
                    if (g.PojazdyCiezarowe[i].Id == id)
                    {
                        g.PojazdyCiezarowe.Remove(g.PojazdyCiezarowe[i]);
                        System.IO.File.Delete(g.Path + "car_000" + id.ToString() + ".car");
                        return true;
                    }
                }
                catch(Exception ex)//TODO obsługa
                {
                    
                }
                
            }
            return false;
        }
        public void edytujSamochod(int id,Garaz g)
        {
            for (int i = 0; i < g.PojazdyCiezarowe.Count + g.PojazdyOsobowe.Count; i++)
            {
                try
                {
                    if (g.PojazdyOsobowe[i].Id == id)
                    {
                        System.IO.File.Delete(g.Path + "/car_000" + id.ToString() + ".car");
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(g.Path + "/car_000" + g.PojazdyOsobowe[i].Id + ".car");
                        sw.WriteLine("osobowy");
                        sw.WriteLine(g.PojazdyOsobowe[i].Kolor);
                        sw.WriteLine(g.PojazdyOsobowe[i].VIN);
                        sw.WriteLine(g.PojazdyOsobowe[i].Marka);
                        sw.WriteLine(g.PojazdyOsobowe[i].Model);
                        sw.WriteLine(g.PojazdyOsobowe[i].KonieMechaniczne.ToString());
                        sw.WriteLine(g.PojazdyOsobowe[i].Wypozyczony.ToString());
                        sw.WriteLine(g.PojazdyOsobowe[i].Id.ToString());
                        sw.WriteLine(g.PojazdyOsobowe[i].Datawy.ToString());
                        sw.WriteLine(g.PojazdyOsobowe[i].IloscPasazerow.ToString());
                        sw.WriteLine(g.PojazdyOsobowe[i].Cena.ToString());
                        sw.Close();

                    }
                    if (g.PojazdyCiezarowe[i].Id == id)
                    {
                        System.IO.File.Delete(g.Path + "car_000" + id.ToString() + ".car");
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(g.Path + "/car_000" + g.PojazdyCiezarowe[i].Id + ".car");
                        sw.WriteLine("ciezarowy");
                        sw.WriteLine(g.PojazdyCiezarowe[i].Kolor);
                        sw.WriteLine(g.PojazdyCiezarowe[i].VIN);
                        sw.WriteLine(g.PojazdyCiezarowe[i].Marka);
                        sw.WriteLine(g.PojazdyCiezarowe[i].Model);
                        sw.WriteLine(g.PojazdyCiezarowe[i].KonieMechaniczne.ToString());
                        sw.WriteLine(g.PojazdyCiezarowe[i].Wypozyczony.ToString());
                        sw.WriteLine(g.PojazdyCiezarowe[i].Id.ToString());
                        sw.WriteLine(g.PojazdyCiezarowe[i].Datawy.ToString());
                        sw.WriteLine(g.PojazdyCiezarowe[i].Ladownosc.ToString());
                        sw.WriteLine(g.PojazdyCiezarowe[i].Cena.ToString());
                        sw.Close();
                    }
                }
                catch (Exception ex)//TODO obsługa
                {

                }

            }
        }

    }
}
