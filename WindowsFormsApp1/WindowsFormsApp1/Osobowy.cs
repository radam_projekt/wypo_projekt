﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Osobowy : Pojazd
    {
        private int iloscPasazerow;

        public Osobowy(string kolor, string vIN, string marka, string model, int konieMechaniczne, bool wypozyczony, int id, DateTime datawy,double cena ,int iloscPasazerow) : base(kolor, vIN, marka, model, konieMechaniczne, wypozyczony, id, datawy,cena)
        {
            this.iloscPasazerow = iloscPasazerow;
        }
        public int IloscPasazerow { get => iloscPasazerow; set => iloscPasazerow = value; }
    }
}
