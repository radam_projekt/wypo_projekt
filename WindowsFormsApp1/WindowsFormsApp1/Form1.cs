﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Resources;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Garaz garaz = null;
        List<Pracownik> listaPracownikow = null;
        List<Klient> listaKlientow = null;
        Klient klient=null;
        Pracownik pracownik=null;
        string pathToPojazdy = Application.StartupPath + "/bazy/pojazdy";
        string pathToLudzie = Application.StartupPath + "/bazy/ludzie";
        public Form1()
        {
            InitializeComponent();
            foreach(Panel pnl in this.Controls)
            {
                pnl.Location = new Point(0, 0);
            }
            this.MaximumSize = Size;
            this.MinimumSize = Size;
            this.Icon = Properties.Resources.rent_a_car_rsJ_icon;
            panelLog.BringToFront();
            garaz = new Garaz(pathToPojazdy);
            wyswietlSamochody();

            listaPracownikow = new List<Pracownik>();
            listaKlientow = new List<Klient>();
            uzupelnijListyPracownikowIKlientow();
            

        }

        private void uzupelnijListyPracownikowIKlientow()
        {
            foreach (string file in Directory.EnumerateFiles(Application.StartupPath + "/bazy/ludzie", "*.txt"))
            {
                int id;
                int prawa; // ( 0 niema, 1 ma)
                string imie, nazwisko, dowod, login, haslo;
                int wiek;
                StreamReader sr = new StreamReader(file);


                id = Convert.ToInt16(sr.ReadLine());
                prawa = Convert.ToInt16(sr.ReadLine());
                imie = sr.ReadLine();
                nazwisko = sr.ReadLine();
                dowod = sr.ReadLine();
                login = sr.ReadLine();
                haslo = sr.ReadLine();
                wiek = Convert.ToInt16(sr.ReadLine());

                if (Convert.ToBoolean(prawa))
                {
                    Pracownik pr = new Pracownik(imie, nazwisko, dowod, login, haslo, Convert.ToInt16(wiek), id);
                    listaPracownikow.Add(pr);
                }
                else
                {
                    List<int> idWyporzyczonychPojazdow = new List<int>();
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!String.IsNullOrEmpty(line))
                        idWyporzyczonychPojazdow.Add(Convert.ToInt16(line));
                    }

                    Klient k = new Klient(imie, nazwisko, dowod, login, haslo, Convert.ToInt16(wiek), id, idWyporzyczonychPojazdow);
                    listaKlientow.Add(k);
                }
                sr.Close();
            }
        }
        void wyswietlSamochody()
        {
            CzyscKontener(panelPrzeg);
            int i = 0;
            foreach (Osobowy po in garaz.PojazdyOsobowe)
            {
                Panel kafelek = new Panel { Parent = panelPrzeg, Visible = true, BackColor = Color.Beige, Size = new Size(400, 100), Location = new Point(0, i * 100),BorderStyle = BorderStyle.FixedSingle, };
                Label lnazwa = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = po.Marka + "    " + po.Model,Font = new Font("Arial",11,FontStyle.Bold) ,Location = new Point(10, 10) };
                Label linfo = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = "kolor: " + po.Kolor + "\r\nliczba miejsc:" + po.IloscPasazerow, Location = new Point(10, 40), Font = new Font("Arial", 9, FontStyle.Regular) };
                Label ldostepny = new Label { Parent = kafelek, Visible = true, AutoSize = true, Location = new Point(250, 10),Font = new Font("Arial",12,FontStyle.Bold) };
                Button bwypozycz = new Button { Parent = kafelek, AutoSize = true, Location = new Point(250, 30), Text = "Wypozycz",Font = new Font("Arial", 11, FontStyle.Regular), BackColor = SystemColors.Control };
                bwypozycz.Click += delegate { BwypozyczClicked(po.Id); };
                Label lcena = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = "Cena " + po.Cena + "zł za dzień", Font = new Font("Arial", 11, FontStyle.Regular), Location = new Point(250, 60) };
            
                if (po.Wypozyczony)
                {
                    ldostepny.Text = "Wypozyczony";
                    ldostepny.ForeColor = Color.DarkGray;
                    bwypozycz.Visible = false;
                }
                else
                {
                    ldostepny.Text = "Dostępny!";
                    ldostepny.ForeColor = Color.LimeGreen;
                    bwypozycz.Visible = true;
                }

                if(pracownik!=null)
                {
                    bwypozycz.Visible = false;
                    linfo.Text += " ID:" + po.Id;
                }
                i++;
            }
            foreach (Ciezarowy pc in garaz.PojazdyCiezarowe)
            {

                Panel kafelek = new Panel { Parent = panelPrzeg, Visible = true, BackColor = Color.PowderBlue, Size = new Size(400, 100), Location = new Point(0, i * 100), BorderStyle = BorderStyle.FixedSingle };
                Label lnazwa = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = pc.Marka + "    " + pc.Model, Font = new Font("Arial", 11, FontStyle.Bold), Location = new Point(10, 10) };
                Label linfo = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = "kolor: " + pc.Kolor + "\r\nładowność:" + pc.Ladownosc+"t", Location = new Point(10, 40), Font = new Font("Arial", 9, FontStyle.Regular) };
                Label ldostepny = new Label { Parent = kafelek, Visible = true, AutoSize = true, Location = new Point(250, 10), Font = new Font("Arial", 12, FontStyle.Bold) };
                Button bwypozycz = new Button { Parent = kafelek, AutoSize = true, Location = new Point(250, 30), Text = "Wypozycz", Font = new Font("Arial", 11, FontStyle.Regular),BackColor = SystemColors.Control };
                bwypozycz.Click += delegate { BwypozyczClicked(pc.Id); };
                Label lcena = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = "Cena " + pc.Cena + "zł za dzień", Font = new Font("Arial", 11, FontStyle.Regular), Location = new Point(250, 60) };
                if (pc.Wypozyczony)
                {
                    ldostepny.Text = "Wypozyczony";
                    bwypozycz.Visible = false;
                }
                else
                {
                    ldostepny.Text = "Dostępny!";
                    bwypozycz.Visible = true;
                }

                if (pracownik != null)
                {
                    bwypozycz.Visible = false;
                    linfo.Text += " ID:" + pc.Id;
                }
                i++;
            }
            foreach(Control ctr in panelPrzeg.Controls)
            {
                ctr.Refresh();
                ctr.BringToFront();
            }         
            this.Refresh();
            panelPrzeg.Refresh();
        }

        private void dodajKlientaDoBazy() // TODO
        {
            try
            {
                StreamWriter sw = new StreamWriter(pathToLudzie + "/osoba" + klient.Id + ".txt");
                sw.WriteLine(klient.Id);
                sw.WriteLine(0);
                sw.WriteLine(klient.Imie);
                sw.WriteLine(klient.Nazwisko);
                sw.WriteLine(klient.NumerISeriaDowodu);
                sw.WriteLine(klient.Login);
                sw.WriteLine(klient.Haslo);
                sw.WriteLine(klient.Wiek);
                panelMain.BringToFront();

                sw.Close();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                panelLog.BringToFront();
            }
        
        }

        private void BwypozyczClicked(int id)
        {
            foreach(Osobowy po in garaz.PojazdyOsobowe)
            {
                if(po.Id==id)
                {
                    po.Wypozyczony = true;
                    po.Datawy = DateTime.Now;
                    garaz.aktualizuj(po.Id,true);
                }
            }
            foreach(Ciezarowy pc in garaz.PojazdyCiezarowe)
            {
                if(pc.Id==id)
                {
                    pc.Wypozyczony = true;
                    pc.Datawy = DateTime.Now;
                    garaz.aktualizuj(pc.Id, false);
                }
            }
            klient.wypozycz(id);
            StreamWriter sw = new StreamWriter(pathToLudzie + "/osoba" + klient.Id + ".txt", true);
            sw.WriteLine(id.ToString());
            sw.Close();
            CzyscKontener(panelPrzeg);
            wyswietlSamochody();
            CzyscKontener(panelWprzeg);
            labelMwypozyczone.Text = "Wypozyczonych:" + klient.Wypozyczone.Count + " aut";

            panelPrzeg.AutoScrollPosition = new Point(0, 0);
            
        }

        private void CzyscKontener(Control kontener)
        {
            foreach (Control ctr in kontener.Controls)
            {
                ctr.Dispose();
            }
            kontener.Refresh();
        }

        private void buttonLzaloguj_Click(object sender, EventArgs e)
        {
            string login = textBoxLlogin.Text;
            string haslo = textBoxLhaslo.Text;

            var klient1 = listaKlientow.Find(x => x.Login == login);
            var pracownik1 = listaPracownikow.Find(x => x.Login == login);

            if (klient1 != null  && klient1.Haslo == haslo)
                {
                klient = klient1;
                panelMain.BringToFront();
                panelUstAdmin.Visible = false;
                labelMuser.Text = "Witaj: "+klient.Imie + " " + klient.Nazwisko;
                labelMwypozyczone.Text ="Wypozyczonych:"+ klient.Wypozyczone.Count + " aut";
                buttonMSamochody.Visible = true;
                textBoxUsunuId.Text = klient.Id.ToString(); ;
                if (klient.Wypozyczone.Count != 0)
                {
                    labelMZalegasz.Visible = true;
                    labelMzalegasz2.Visible = true;
                }
            }
            else if(pracownik1!=null && pracownik1.Haslo==haslo)
            {
                pracownik = pracownik1;
                panelMain.BringToFront();
                panelUstAdmin.Visible = true;
                buttonMSamochody.Visible = false;
            }
            else
            { MessageBox.Show("Niepoprawny login lub haslo"); }

            wyswietlSamochody();
        }
        private void textBoxLhaslo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonLzaloguj_Click(this, new EventArgs());
                panelPrzeg.Focus();
            }
        }

        private void buttonLzarejestruj_Click(object sender, EventArgs e)
        {
            panelRejest.BringToFront();
        }

        private void buttonUsuns_Click(object sender, EventArgs e)
        {
            int id;
            Int32.TryParse(textBoxUsunsId.Text, out id);
            if(pracownik.usunSamochod(id, garaz))
            {
                MessageBox.Show("Pomyslnie usunieto");
                textBoxUsunsId.Text = null;
            }
            else
            {
                MessageBox.Show("Nie udało sie usunąć");
            }
            CzyscKontener(panelPrzeg);
            wyswietlSamochody();
        }

        private void buttonMust_Click(object sender, EventArgs e)
        {
            panelUst.BringToFront();
            if (klient != null) textBoxUsunuId.Text = klient.Id.ToString();
        }

        private void buttonUwroc_Click(object sender, EventArgs e)
        {
            panelMain.BringToFront();
        }

        private void buttonRanuluj_Click(object sender, EventArgs e)
        {
            panelLog.BringToFront();    // już działa
        }

        private void buttonRzarestruj_Click(object sender, EventArgs e)
        {
            string imie, nazwisko, login, haslo1, haslo2, dowod;
            int wiek, id;

            imie = textBoxRimie.Text;
            nazwisko = textBoxRnazwisko.Text;
            login = textBoxRlogin.Text;
            haslo1 = textBoxRhaslo.Text;
            haslo2 = textBoxRhaslo2.Text;
            dowod = textBoxRdowod.Text;
            id = listaKlientow.Count() + listaPracownikow.Count();
            wiek = DateTime.Now.Year - dateTimePicker1.Value.Year;


            if (imie.Length < 1 || nazwisko.Length < 1 || login.Length < 1 || haslo1.Length < 1 || dowod.Length < 1)
            {
                MessageBox.Show("Uzupelnij wszystkie rubryki");
            }
            else if (haslo1 != haslo2)
            {
                MessageBox.Show("Podane hasla nie sa identyczne");
            }
            else if (listaKlientow.Find(x => x.Login==login)!= null)
            {
                MessageBox.Show("Istnieje juz ktos o padanym loginie");
            }
            else
            {
                List<int> lista = new List<int> (); 
                klient = new Klient(imie, nazwisko, dowod, login, haslo1, Convert.ToInt16(wiek), id, lista);
                listaKlientow.Add(klient);

                textBoxUsunuId.Text = klient.Id.ToString();
                panelUstAdmin.Visible = false;
                labelMuser.Text = "Witaj: " + klient.Imie + " " + klient.Nazwisko;
                labelMwypozyczone.Text = "Wypozyczonych:" + klient.Wypozyczone.Count + " aut";
                buttonMSamochody.Visible = true;

                dodajKlientaDoBazy();
            }
        }

        private void buttonUedytuj_Click(object sender, EventArgs e)
        {
            foreach (Control ctrl in groupBoxEdytuj.Controls)
            {
                
                if(ctrl is TextBox)
                {
                    ctrl.Text = null;
                    ctrl.BackColor = SystemColors.Window;
                }
                else if(ctrl is NumericUpDown)
                {
                    ((NumericUpDown)(ctrl)).Value = 0;
                    ctrl.BackColor = SystemColors.Window;
                }
                else if(ctrl is DateTimePicker)
                {
                    ((DateTimePicker)(ctrl)).Value = DateTime.Now;
                    ctrl.BackColor = SystemColors.Window;
                }
                else
                {
                    ctrl.BackColor = SystemColors.Control;
                }
            }
            groupBoxEdytuj.Text = "Edytuj Pojazd";
            panelEdytuj.BringToFront();
            int id;
            Int32.TryParse(textBoxUedytujid.Text, out id);

            for (int i = 0; i < garaz.PojazdyCiezarowe.Count + garaz.PojazdyOsobowe.Count; i++)
            {
                try
                {
                    if (garaz.PojazdyOsobowe[i].Id == id)
                    {
                        radioButtonEosobowy.Checked = true;
                        textBoxEkolor.Text = garaz.PojazdyOsobowe[i].Kolor;
                        textBoxEvin.Text = garaz.PojazdyOsobowe[i].VIN;
                        textBoxEmarka.Text = garaz.PojazdyOsobowe[i].Marka;
                        textBoxEmodel.Text = garaz.PojazdyOsobowe[i].Model;
                        numericUpDownEkonie.Value = garaz.PojazdyOsobowe[i].KonieMechaniczne;
                        if (garaz.PojazdyOsobowe[i].Wypozyczony) checkBoxEwypozyczony.Checked = true;
                        else checkBoxEwypozyczony.Checked = false;
                        numericUpDownEid.Value = (decimal)garaz.PojazdyOsobowe[i].Id;
                        dateTimePickerEdata.Value = garaz.PojazdyOsobowe[i].Datawy;
                        numericUpDownEilosc.Value = (decimal)garaz.PojazdyOsobowe[i].IloscPasazerow;
                        numericUpDownEcena.Value = (decimal)garaz.PojazdyOsobowe[i].Cena;
                    }
                    if (garaz.PojazdyCiezarowe[i].Id == id)
                    {
                        radioButtonEciezarowy.Checked = true;
                        textBoxEkolor.Text = garaz.PojazdyCiezarowe[i].Kolor;
                        textBoxEvin.Text = garaz.PojazdyCiezarowe[i].VIN;
                        textBoxEmarka.Text = garaz.PojazdyCiezarowe[i].Marka;
                        textBoxEmodel.Text = garaz.PojazdyCiezarowe[i].Model;
                        numericUpDownEkonie.Value = garaz.PojazdyCiezarowe[i].KonieMechaniczne;
                        if (garaz.PojazdyCiezarowe[i].Wypozyczony) checkBoxEwypozyczony.Checked = true;
                        else checkBoxEwypozyczony.Checked = false;
                        numericUpDownEid.Value = garaz.PojazdyCiezarowe[i].Id;
                        dateTimePickerEdata.Value = garaz.PojazdyCiezarowe[i].Datawy;
                        numericUpDownEilosc.Value = (decimal)garaz.PojazdyCiezarowe[i].Ladownosc;
                        numericUpDownEcena.Value = (decimal)garaz.PojazdyCiezarowe[i].Cena;
                    }
                }
                catch (Exception ex)//TODO obsługa
                {
                }
            }
        }

        private void buttonUdodaj_Click(object sender, EventArgs e)
        {
            groupBoxEdytuj.Text = "Dodaj Pojazd";
            panelEdytuj.BringToFront();
            foreach(Control ctrl in groupBoxEdytuj.Controls)
            {
                ctrl.BackColor = SystemColors.Control;
                if(ctrl is TextBox)
                {
                    ctrl.Text = null;
                }
                if(ctrl is NumericUpDown)
                {
                    ((NumericUpDown)(ctrl)).Value = 0;
                }
                if(ctrl is DateTimePicker)
                {
                    ((DateTimePicker)(ctrl)).Value = DateTime.Now;
                }
            }
            radioButtonEosobowy.Checked = true;
        }

        private void radioButtonEosobowy_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonEosobowy.Checked == true) labelEilosc.Text = "Siedzeń:";
            else labelEilosc.Text = "Ładowność(t):";
        }

        private void buttonEanuluj_Click(object sender, EventArgs e)
        {
            panelUst.BringToFront();
        }

        private void buttonEzatwierdz_Click(object sender, EventArgs e)
        {
            bool poprawne = true;
            foreach(Control ctrl in groupBoxEdytuj.Controls)
            {
                if(ctrl is TextBox)
                {
                    if(((TextBox)(ctrl)).Text == null)
                    {
                        poprawne = false;
                        break;
                    }
                }
                if(ctrl is NumericUpDown)
                {
                    if (((NumericUpDown)(ctrl)).Value <=0)
                    {
                        poprawne = false;
                        break;
                    }
                }

            }
            if (!radioButtonEciezarowy.Checked && !radioButtonEosobowy.Checked) poprawne = false;
            if(poprawne)
            {
                if (groupBoxEdytuj.Text == "Dodaj Pojazd")
                {
                    Pojazd p = new Pojazd(textBoxEkolor.Text, textBoxEvin.Text, textBoxEmarka.Text, textBoxEmodel.Text, (Int32)numericUpDownEkonie.Value, true, (Int32)numericUpDownEid.Value, dateTimePickerEdata.Value,(double)numericUpDownEcena.Value);
                    if (radioButtonEosobowy.Checked)
                    {
                        garaz.dodajPojazd(p, true, (Int32)numericUpDownEilosc.Value);
                    }
                    else if (radioButtonEciezarowy.Checked)
                    {
                        garaz.dodajPojazd(p, false, (Int32)numericUpDownEilosc.Value);
                    }
                    MessageBox.Show("dodano Pojazd");
                }
                else
                { 
                    if(radioButtonEosobowy.Checked)
                    {
                        foreach(Osobowy po in garaz.PojazdyOsobowe)
                        {
                            if(po.Id==(int)numericUpDownEid.Value)
                            {
                                po.Kolor = textBoxEkolor.Text;
                                po.VIN = textBoxEvin.Text;
                                po.Marka = textBoxEmarka.Text;
                                po.Model = textBoxEmodel.Text;
                                po.KonieMechaniczne = (int)numericUpDownEkonie.Value;
                                po.Id = (int)numericUpDownEid.Value;
                                po.Datawy = dateTimePickerEdata.Value;
                                po.IloscPasazerow = (int)numericUpDownEilosc.Value;
                                po.Wypozyczony = !checkBoxEwypozyczony.Checked;
                                po.Cena = (double)numericUpDownEcena.Value;
                                pracownik.edytujSamochod(po.Id,garaz);
                            }
                        }
                        
                    }
                    else
                    {
                        foreach(Ciezarowy pc in garaz.PojazdyCiezarowe)
                        {
                            if(pc.Id==(int)numericUpDownEid.Value)
                            {
                                pc.Kolor = textBoxEkolor.Text;
                                pc.VIN = textBoxEvin.Text;
                                pc.Marka = textBoxEmarka.Text;
                                pc.Model = textBoxEmodel.Text;
                                pc.KonieMechaniczne = (int)numericUpDownEkonie.Value;
                                pc.Id = (int)numericUpDownEid.Value;
                                pc.Datawy = dateTimePickerEdata.Value;
                                pc.Ladownosc = (double)numericUpDownEilosc.Value;
                                pc.Wypozyczony = !checkBoxEwypozyczony.Checked;
                                pc.Cena = (double)numericUpDownEcena.Value;
                                pracownik.edytujSamochod(pc.Id, garaz);
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Wprowadz poprawne dane");
            }

            wyswietlSamochody();
            panelUst.BringToFront();
        }

        private void buttonMSamochody_Click(object sender, EventArgs e)
        {
            panelWypozyczone.BringToFront();
            WyswietlWypozyczone();
        }

        private void buttonWwroc_Click(object sender, EventArgs e)
        {
            panelMain.BringToFront();
            CzyscKontener(panelWprzeg);
            CzyscKontener(panelPrzeg);
            wyswietlSamochody();
        }

        private void WyswietlWypozyczone()
        {
            CzyscKontener(panelWprzeg);
            int i = 0;
            foreach(int id in klient.Wypozyczone)
            {
                
                Osobowy os = garaz.PojazdyOsobowe.Find(po => po.Id == id);
                if(os!=null)
                {
                    TimeSpan ts = DateTime.Now - os.Datawy;
                    double cena = Math.Round(10 + ((ts.TotalDays) * os.Cena),2);
                    Panel kafelek = new Panel { Parent = panelWprzeg, Visible = true, BackColor = Color.Beige, Size = new Size(360, 100), Location = new Point(0, i * 100), BorderStyle = BorderStyle.FixedSingle, };
                    Label lnazwa = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = os.Marka + "    " + os.Model, Font = new Font("Arial", 11, FontStyle.Bold), Location = new Point(10, 10) };
                    Label linfo = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = "kolor: " + os.Kolor + "\r\nliczba miejsc:" + os.IloscPasazerow+"\r\nDo zapłaty:"+cena+"zł", Location = new Point(10, 40), Font = new Font("Arial", 9, FontStyle.Regular) };
                    Label ldata = new Label { Parent = kafelek, Visible = true, AutoSize = true, Location = new Point(150, 10), Font = new Font("Arial", 10, FontStyle.Bold), Text = "Wypozyczono:"+os.Datawy.ToString() };
                    Button boddaj = new Button { Parent = kafelek, AutoSize = true, Location = new Point(250, 30), Text = "Oddaj", Font = new Font("Arial", 11, FontStyle.Regular) };
                    boddaj.Click += delegate { BoddajClicked(os.Id); };
                    i++;
                }
                Ciezarowy ci = garaz.PojazdyCiezarowe.Find(pc => pc.Id == id);
                if(ci!=null)
                {
                    TimeSpan ts = DateTime.Now - ci.Datawy;
                    double cena = Math.Round(10 + ((ts.TotalDays) * ci.Cena), 2);
                    Panel kafelek = new Panel { Parent = panelWprzeg, Visible = true, BackColor = Color.Beige, Size = new Size(360, 100), Location = new Point(0, i * 100), BorderStyle = BorderStyle.FixedSingle, };
                    Label lnazwa = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = ci.Marka + "    " + ci.Model, Font = new Font("Arial", 11, FontStyle.Bold), Location = new Point(10, 10) };
                    Label linfo = new Label { Parent = kafelek, Visible = true, AutoSize = true, Text = "kolor: " + ci.Kolor + "\r\nładowność:" + ci.Ladownosc + "\r\nDo zapłaty:" + cena + "zł", Location = new Point(10, 40), Font = new Font("Arial", 9, FontStyle.Regular) };
                    Label ldata = new Label { Parent = kafelek, Visible = true, AutoSize = true, Location = new Point(150, 10), Font = new Font("Arial", 10, FontStyle.Bold), Text = "Wypozyczono:" + ci.Datawy.ToString() };
                    Button boddaj = new Button { Parent = kafelek, AutoSize = true, Location = new Point(250, 30), Text = "Oddaj", Font = new Font("Arial", 11, FontStyle.Regular) };
                    boddaj.Click += delegate { BoddajClicked(ci.Id); };
                    i++;
                }
            }
            foreach (Control ctr in panelWprzeg.Controls)
            {
                ctr.Refresh();
                ctr.BringToFront();
            }
            panelPrzeg.Refresh();
            this.Refresh();
        }

        private void BoddajClicked(int id)
        {
            
            double cena=klient.oddaj(id,garaz);
            GenerujFakture(klient, cena, id);
            string linia = null;

            using (StreamReader reader = new System.IO.StreamReader(pathToLudzie + "/osoba" + klient.Id + ".txt"))
            {
                int numerlinii = 0;
                while ((linia = reader.ReadLine()) != null)
                {
                    if (String.Compare(linia, id.ToString())==0)
                    {
                        break;
                    }
                    numerlinii++;
                }
                reader.Close();
                var file = new List<string>(System.IO.File.ReadAllLines(pathToLudzie + "/osoba" + klient.Id + ".txt"));
                file.RemoveAt(numerlinii);
                File.WriteAllLines(pathToLudzie + "/osoba" + klient.Id + ".txt",file.ToArray());
            }
            CzyscKontener(panelWprzeg);
            this.Refresh();
            foreach(Osobowy po in garaz.PojazdyOsobowe)
            {
                if(po.Id==id)
                {
                    po.Wypozyczony = false;
                    po.Datawy = Convert.ToDateTime(null);
                    File.Delete(garaz.Path + "/car_000" + id.ToString() + ".car");
                    StreamWriter sw = new StreamWriter(garaz.Path + "/car_000" + po.Id + ".car");
                    sw.WriteLine("osobowy");
                    sw.WriteLine(po.Kolor);
                    sw.WriteLine(po.VIN);
                    sw.WriteLine(po.Marka);
                    sw.WriteLine(po.Model);
                    sw.WriteLine(po.KonieMechaniczne.ToString());
                    sw.WriteLine(po.Wypozyczony.ToString());
                    sw.WriteLine(po.Id.ToString());
                    sw.WriteLine(po.Datawy.ToString());
                    sw.WriteLine(po.IloscPasazerow.ToString());
                    sw.WriteLine(po.Cena.ToString());
                    sw.Close();
                    break;
                }
            }
            foreach(Ciezarowy pc in garaz.PojazdyCiezarowe)
            {
                if(pc.Id==id)
                {
                    pc.Wypozyczony = false;
                    pc.Datawy = Convert.ToDateTime(null);
                    File.Delete(garaz.Path + "/car_000" + id.ToString() + ".car");
                    StreamWriter sw = new StreamWriter(garaz.Path + "/car_000" + pc.Id + ".car");
                    sw.WriteLine("ciezarowy");
                    sw.WriteLine(pc.Kolor);
                    sw.WriteLine(pc.VIN);
                    sw.WriteLine(pc.Marka);
                    sw.WriteLine(pc.Model);
                    sw.WriteLine(pc.KonieMechaniczne.ToString());
                    sw.WriteLine(pc.Wypozyczony.ToString());
                    sw.WriteLine(pc.Id.ToString());
                    sw.WriteLine(pc.Datawy.ToString());
                    sw.WriteLine(pc.Ladownosc.ToString());
                    sw.WriteLine(pc.Cena.ToString());
                    sw.Close();
                    break;
                }
            }
            CzyscKontener(panelPrzeg);
            WyswietlWypozyczone();
            this.Refresh();
            panelPrzeg.Refresh();
            labelMwypozyczone.Text = "Wypozyczonych:"+klient.Wypozyczone.Count + " aut";

            if (klient.Wypozyczone.Count != 0)
            {
                labelMZalegasz.Visible = true;
                labelMzalegasz2.Visible = true;
            }
            foreach (Control ctr in panelPrzeg.Controls)
            {
                ctr.Refresh();
                ctr.BringToFront();
            }
        }

        void GenerujFakture(Klient k,double cena,int id)
        {
            Osobowy os = null;
            Ciezarowy ci = null;
            foreach(Osobowy po in garaz.PojazdyOsobowe)
            {
                if(po.Id==id)
                {
                    os = po;
                    break;
                }
            }
            foreach(Ciezarowy pc in garaz.PojazdyCiezarowe)
            {
                if(pc.Id==id)
                {
                    ci = pc;
                    break;
                }
            }
            Random rnd = new Random();
            StreamWriter sw = new StreamWriter(Application.StartupPath + "/rachunek" + rnd.Next(1000,9999).ToString()+".txt");
            for (int i = 0; i < 45; i++)
            {
                sw.Write(" ");
            }
            sw.WriteLine("DO ZAPŁATY");
            sw.Write("Najemca: " + k.Imie + " " + k.Nazwisko);
            for(int i=0;i<35;i++)
            {
                sw.Write(" ");
            }
            sw.WriteLine("Wynajmujący: Wypozyczalnia samochodow");
            sw.WriteLine("Legitymujący sie dowodem: " + k.NumerISeriaDowodu);
            sw.WriteLine("\t");
            sw.WriteLine("\t");
            for (int i = 0; i < 100; i++)
            {
                sw.Write("#");
            }
            sw.WriteLine();
            if(os!=null)
            {
                sw.WriteLine(os.Marka + " " + os.Model + "\t\t Wypozyczony od" + os.Datawy.ToShortDateString().ToString() + " do " + DateTime.Now.ToShortDateString().ToString() + "\t\t Cena 10zł +" + os.Cena + "zł za dzień");
            }
            else
            {
                sw.WriteLine(ci.Marka + " " + ci.Model + "\t\t Wypozyczony od" + ci.Datawy.ToShortDateString().ToString() + " do " + DateTime.Now.ToShortDateString().ToString() + "\t\t Cena 80zł +" + ci.Cena + "zł za dzień");
            }
            sw.WriteLine("\t");
            sw.WriteLine("\t");
            sw.WriteLine("ŁĄCZNIE DO ZAPŁATY:");
            sw.WriteLine((Math.Round(cena, 2)).ToString());
            sw.Close();
        }


        private void buttonMwyloguj_Click_1(object sender, EventArgs e)
        {
            panelLog.BringToFront();
            klient = null;
            pracownik = null;

            textBoxLhaslo.ResetText();
            textBoxLlogin.ResetText();
        }

        private void buttonUsunu_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(textBoxUsunuId.Text))
                {
                    throw new System.ArgumentException("Musisz podać jakieś ID");
                }
                else if(!textBoxUsunuId.Text.All(char.IsDigit))
                {
                    throw new System.ArgumentException("Musisz podac liczbe");
                }
                else 
                {
                    var KLIENT = listaKlientow.Find(x => x.Id == Convert.ToInt16(textBoxUsunuId.Text));
                    listaKlientow.Remove(KLIENT);

                    int idZPliku;

                    foreach (string file in Directory.EnumerateFiles(Application.StartupPath + "/bazy/ludzie", "*.txt"))
                    {
                        StreamReader sr = new StreamReader(file);

                        idZPliku = Convert.ToInt16(sr.ReadLine());
                        sr.Close();
                        if (idZPliku == KLIENT.Id)
                        {
                            File.Delete(file);
                            throw new System.ArgumentException("Pomyslnie usunieto uzytkownika");
                        }
                    }

                    throw new System.ArgumentException("Nie znaleziono uzytkownika o takim ID");

                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(textBox1.Text) || !textBox1.Text.All(char.IsDigit))
            {
                MessageBox.Show("Musisz podac jakies id");
            }
            else if((listaKlientow.Find(x => x.Id == Convert.ToInt16(textBox1.Text)) == null))
            {
                MessageBox.Show("Nie Klienta o podanym Id");
            }
            else
            {
                var KLIENT = listaKlientow.Find(x => x.Id == Convert.ToInt16(textBox1.Text));
                textBoxEdImie.Text = KLIENT.Imie;
                textBoxEdNazw.Text = KLIENT.Nazwisko;
                textBoxEdLog.Text = KLIENT.Login;
                textBoxEdHas.Text = KLIENT.Haslo;
                textBoxEdN_i_S.Text = KLIENT.NumerISeriaDowodu;
                panel1.BringToFront();
            }
        }

        private void wyczyscTextZPaneluEducja()
        {

            textBoxEdImie.ResetText();
            textBoxEdNazw.ResetText();
            textBoxEdN_i_S.ResetText();
            textBoxEdLog.ResetText();
            textBoxEdHas.ResetText();
            dateTimePickerEdycja.ResetText();
            panelUst.BringToFront();
            textBox1.ResetText(); // czysci textBoxa przy przycisku wywolujacym
        }


        private void buttonEdAnuluj_Click(object sender, EventArgs e)
        {
            wyczyscTextZPaneluEducja();

            panelUst.BringToFront();
        }

        private void buttonEdZapisz_Click(object sender, EventArgs e) // duplikacja kodu ze wczytywania
        {

            var KLIENT = listaKlientow.Find(x => x.Id == Convert.ToInt16(textBox1.Text));  //podczas podawania id jest sprawdzane czy 

            foreach (string file in Directory.EnumerateFiles(Application.StartupPath + "/bazy/ludzie", "*.txt"))
            {

                int id;
                int prawa; // ( 0 niema, 1 ma)
                string imie, nazwisko, dowod, login, haslo;
                int wiek;
                StreamReader sr = new StreamReader(file);


                id = Convert.ToInt16(sr.ReadLine());
                prawa = 0;
                sr.ReadLine();
                imie = sr.ReadLine();
                nazwisko = sr.ReadLine();
                dowod = sr.ReadLine();
                login = sr.ReadLine();
                haslo = sr.ReadLine();
                wiek = Convert.ToInt16(sr.ReadLine());

                List<int> idWyporzyczonychPojazdow = new List<int>();
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if(!String.IsNullOrEmpty(line))
                    idWyporzyczonychPojazdow.Add(Convert.ToInt16(line));
                }

                sr.Close();

                if( login == KLIENT.Login && haslo == KLIENT.Haslo )
                { 

                StreamWriter sw = new StreamWriter(file);

                sw.WriteLine(KLIENT.Id);
                sw.WriteLine(prawa);
                sw.WriteLine(textBoxEdImie.Text);
                sw.WriteLine(textBoxEdNazw.Text);
                sw.WriteLine(textBoxEdN_i_S.Text);
                sw.WriteLine(textBoxEdLog.Text);
                sw.WriteLine(textBoxEdHas.Text);
                sw.WriteLine(DateTime.Now.Year - dateTimePicker1.Value.Year);

                foreach(int numer in idWyporzyczonychPojazdow)
                {
                    sw.WriteLine(numer);
                }

                sw.Close();

                }
            }





                wyczyscTextZPaneluEducja();

            panelUst.BringToFront();
        }

        private void buttonUusunkonto_Click(object sender, EventArgs e)
        {
            if(klient!=null&&klient.Wypozyczone.Count!=0)
            {
                MessageBox.Show("Zanim usuniesz konto, zwróć wszystkie samochody");
            }
            else if(klient!=null&&klient.Wypozyczone.Count==0)
            {
                buttonUsunu_Click(this, new EventArgs());
                panelLog.BringToFront();
            }
            
        }
    }
}
