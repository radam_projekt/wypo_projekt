﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Ciezarowy : Pojazd
    {
        private double ladownosc;
        public Ciezarowy(string kolor, string vIN, string marka, string model, int konieMechaniczne, bool wypozyczony, int id, DateTime datawy,double cena, double ladownosc) : base(kolor, vIN, marka, model, konieMechaniczne, wypozyczony, id, datawy,cena)
        {
            this.ladownosc = ladownosc;
        }

        public double Ladownosc { get => ladownosc; set => ladownosc = value; }
    }
}
