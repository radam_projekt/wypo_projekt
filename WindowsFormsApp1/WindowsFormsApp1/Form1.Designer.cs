﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLog = new System.Windows.Forms.Panel();
            this.groupBoxLogin = new System.Windows.Forms.GroupBox();
            this.labelLhaslo = new System.Windows.Forms.Label();
            this.labelLlogin = new System.Windows.Forms.Label();
            this.textBoxLlogin = new System.Windows.Forms.TextBox();
            this.buttonLzarejestruj = new System.Windows.Forms.Button();
            this.textBoxLhaslo = new System.Windows.Forms.TextBox();
            this.buttonLzaloguj = new System.Windows.Forms.Button();
            this.panelRejest = new System.Windows.Forms.Panel();
            this.groupBoxRejestruj = new System.Windows.Forms.GroupBox();
            this.buttonRanuluj = new System.Windows.Forms.Button();
            this.buttonRzarestruj = new System.Windows.Forms.Button();
            this.labelRdata = new System.Windows.Forms.Label();
            this.labelRdowod = new System.Windows.Forms.Label();
            this.labelRnazwisko = new System.Windows.Forms.Label();
            this.labelRimie = new System.Windows.Forms.Label();
            this.labelRhaslo2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBoxRdowod = new System.Windows.Forms.TextBox();
            this.textBoxRnazwisko = new System.Windows.Forms.TextBox();
            this.textBoxRimie = new System.Windows.Forms.TextBox();
            this.textBoxRhaslo2 = new System.Windows.Forms.TextBox();
            this.labelRhaslo = new System.Windows.Forms.Label();
            this.labelRlogin = new System.Windows.Forms.Label();
            this.textBoxRhaslo = new System.Windows.Forms.TextBox();
            this.textBoxRlogin = new System.Windows.Forms.TextBox();
            this.panelPrzeg = new System.Windows.Forms.Panel();
            this.panelUst = new System.Windows.Forms.Panel();
            this.groupBoxUkontakt = new System.Windows.Forms.GroupBox();
            this.labelUkontakt = new System.Windows.Forms.Label();
            this.groupBoxUusunkonto = new System.Windows.Forms.GroupBox();
            this.labelUusunkonto = new System.Windows.Forms.Label();
            this.buttonUusunkonto = new System.Windows.Forms.Button();
            this.buttonUwroc = new System.Windows.Forms.Button();
            this.panelUstAdmin = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.idlUstawienia = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBoxUedytujs = new System.Windows.Forms.GroupBox();
            this.buttonUdodaj = new System.Windows.Forms.Button();
            this.buttonUedytuj = new System.Windows.Forms.Button();
            this.textBoxUedytujid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxUsunu = new System.Windows.Forms.GroupBox();
            this.buttonUsunu = new System.Windows.Forms.Button();
            this.textBoxUsunuId = new System.Windows.Forms.TextBox();
            this.labelUsunuId = new System.Windows.Forms.Label();
            this.groupBoxUsuns = new System.Windows.Forms.GroupBox();
            this.buttonUsuns = new System.Windows.Forms.Button();
            this.textBoxUsunsId = new System.Windows.Forms.TextBox();
            this.labelUsunsId = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.labelMzalegasz2 = new System.Windows.Forms.Label();
            this.labelMZalegasz = new System.Windows.Forms.Label();
            this.labelMwypozyczone = new System.Windows.Forms.Label();
            this.buttonMwyloguj = new System.Windows.Forms.Button();
            this.labelMuser = new System.Windows.Forms.Label();
            this.buttonMSamochody = new System.Windows.Forms.Button();
            this.buttonMust = new System.Windows.Forms.Button();
            this.panelEdytuj = new System.Windows.Forms.Panel();
            this.groupBoxEdytuj = new System.Windows.Forms.GroupBox();
            this.numericUpDownEcena = new System.Windows.Forms.NumericUpDown();
            this.labelEcena = new System.Windows.Forms.Label();
            this.labelEilosc = new System.Windows.Forms.Label();
            this.labelEdada = new System.Windows.Forms.Label();
            this.labelEid = new System.Windows.Forms.Label();
            this.labelEwypozyczony = new System.Windows.Forms.Label();
            this.labelEkonie = new System.Windows.Forms.Label();
            this.labelEmodel = new System.Windows.Forms.Label();
            this.labelEmarka = new System.Windows.Forms.Label();
            this.labelEvin = new System.Windows.Forms.Label();
            this.labelEkolor = new System.Windows.Forms.Label();
            this.labelEtyp = new System.Windows.Forms.Label();
            this.buttonEanuluj = new System.Windows.Forms.Button();
            this.buttonEzatwierdz = new System.Windows.Forms.Button();
            this.numericUpDownEilosc = new System.Windows.Forms.NumericUpDown();
            this.dateTimePickerEdata = new System.Windows.Forms.DateTimePicker();
            this.numericUpDownEid = new System.Windows.Forms.NumericUpDown();
            this.checkBoxEwypozyczony = new System.Windows.Forms.CheckBox();
            this.numericUpDownEkonie = new System.Windows.Forms.NumericUpDown();
            this.textBoxEmodel = new System.Windows.Forms.TextBox();
            this.textBoxEmarka = new System.Windows.Forms.TextBox();
            this.textBoxEvin = new System.Windows.Forms.TextBox();
            this.radioButtonEciezarowy = new System.Windows.Forms.RadioButton();
            this.radioButtonEosobowy = new System.Windows.Forms.RadioButton();
            this.textBoxEkolor = new System.Windows.Forms.TextBox();
            this.panelWypozyczone = new System.Windows.Forms.Panel();
            this.panelWprzeg = new System.Windows.Forms.Panel();
            this.buttonWwroc = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerEdycja = new System.Windows.Forms.DateTimePicker();
            this.buttonEdAnuluj = new System.Windows.Forms.Button();
            this.buttonEdZapisz = new System.Windows.Forms.Button();
            this.textBoxEdN_i_S = new System.Windows.Forms.TextBox();
            this.textBoxEdHas = new System.Windows.Forms.TextBox();
            this.textBoxEdLog = new System.Windows.Forms.TextBox();
            this.textBoxEdNazw = new System.Windows.Forms.TextBox();
            this.textBoxEdImie = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelLog.SuspendLayout();
            this.groupBoxLogin.SuspendLayout();
            this.panelRejest.SuspendLayout();
            this.groupBoxRejestruj.SuspendLayout();
            this.panelUst.SuspendLayout();
            this.groupBoxUkontakt.SuspendLayout();
            this.groupBoxUusunkonto.SuspendLayout();
            this.panelUstAdmin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxUedytujs.SuspendLayout();
            this.groupBoxUsunu.SuspendLayout();
            this.groupBoxUsuns.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.panelEdytuj.SuspendLayout();
            this.groupBoxEdytuj.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEcena)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEilosc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEkonie)).BeginInit();
            this.panelWypozyczone.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLog
            // 
            this.panelLog.Controls.Add(this.groupBoxLogin);
            this.panelLog.Location = new System.Drawing.Point(1363, 341);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(800, 600);
            this.panelLog.TabIndex = 0;
            // 
            // groupBoxLogin
            // 
            this.groupBoxLogin.Controls.Add(this.labelLhaslo);
            this.groupBoxLogin.Controls.Add(this.labelLlogin);
            this.groupBoxLogin.Controls.Add(this.textBoxLlogin);
            this.groupBoxLogin.Controls.Add(this.buttonLzarejestruj);
            this.groupBoxLogin.Controls.Add(this.textBoxLhaslo);
            this.groupBoxLogin.Controls.Add(this.buttonLzaloguj);
            this.groupBoxLogin.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxLogin.Location = new System.Drawing.Point(200, 100);
            this.groupBoxLogin.Name = "groupBoxLogin";
            this.groupBoxLogin.Size = new System.Drawing.Size(400, 400);
            this.groupBoxLogin.TabIndex = 4;
            this.groupBoxLogin.TabStop = false;
            this.groupBoxLogin.Text = "Zaloguj się";
            // 
            // labelLhaslo
            // 
            this.labelLhaslo.AutoSize = true;
            this.labelLhaslo.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLhaslo.Location = new System.Drawing.Point(59, 146);
            this.labelLhaslo.Name = "labelLhaslo";
            this.labelLhaslo.Size = new System.Drawing.Size(63, 22);
            this.labelLhaslo.TabIndex = 5;
            this.labelLhaslo.Text = "Hasło:";
            // 
            // labelLlogin
            // 
            this.labelLlogin.AutoSize = true;
            this.labelLlogin.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLlogin.Location = new System.Drawing.Point(59, 100);
            this.labelLlogin.Name = "labelLlogin";
            this.labelLlogin.Size = new System.Drawing.Size(62, 22);
            this.labelLlogin.TabIndex = 4;
            this.labelLlogin.Text = "Login:";
            // 
            // textBoxLlogin
            // 
            this.textBoxLlogin.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxLlogin.Location = new System.Drawing.Point(150, 100);
            this.textBoxLlogin.Name = "textBoxLlogin";
            this.textBoxLlogin.Size = new System.Drawing.Size(152, 28);
            this.textBoxLlogin.TabIndex = 0;
            // 
            // buttonLzarejestruj
            // 
            this.buttonLzarejestruj.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonLzarejestruj.Location = new System.Drawing.Point(50, 300);
            this.buttonLzarejestruj.Name = "buttonLzarejestruj";
            this.buttonLzarejestruj.Size = new System.Drawing.Size(300, 35);
            this.buttonLzarejestruj.TabIndex = 3;
            this.buttonLzarejestruj.Text = "Zarejestruj się";
            this.buttonLzarejestruj.UseVisualStyleBackColor = true;
            this.buttonLzarejestruj.Click += new System.EventHandler(this.buttonLzarejestruj_Click);
            // 
            // textBoxLhaslo
            // 
            this.textBoxLhaslo.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxLhaslo.Location = new System.Drawing.Point(150, 140);
            this.textBoxLhaslo.Name = "textBoxLhaslo";
            this.textBoxLhaslo.PasswordChar = '*';
            this.textBoxLhaslo.Size = new System.Drawing.Size(152, 28);
            this.textBoxLhaslo.TabIndex = 1;
            this.textBoxLhaslo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxLhaslo_KeyDown);
            // 
            // buttonLzaloguj
            // 
            this.buttonLzaloguj.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonLzaloguj.Location = new System.Drawing.Point(150, 180);
            this.buttonLzaloguj.Name = "buttonLzaloguj";
            this.buttonLzaloguj.Size = new System.Drawing.Size(152, 35);
            this.buttonLzaloguj.TabIndex = 2;
            this.buttonLzaloguj.Text = "Zaloguj";
            this.buttonLzaloguj.UseVisualStyleBackColor = true;
            this.buttonLzaloguj.Click += new System.EventHandler(this.buttonLzaloguj_Click);
            // 
            // panelRejest
            // 
            this.panelRejest.Controls.Add(this.groupBoxRejestruj);
            this.panelRejest.Location = new System.Drawing.Point(1080, 579);
            this.panelRejest.Name = "panelRejest";
            this.panelRejest.Size = new System.Drawing.Size(800, 600);
            this.panelRejest.TabIndex = 1;
            // 
            // groupBoxRejestruj
            // 
            this.groupBoxRejestruj.Controls.Add(this.buttonRanuluj);
            this.groupBoxRejestruj.Controls.Add(this.buttonRzarestruj);
            this.groupBoxRejestruj.Controls.Add(this.labelRdata);
            this.groupBoxRejestruj.Controls.Add(this.labelRdowod);
            this.groupBoxRejestruj.Controls.Add(this.labelRnazwisko);
            this.groupBoxRejestruj.Controls.Add(this.labelRimie);
            this.groupBoxRejestruj.Controls.Add(this.labelRhaslo2);
            this.groupBoxRejestruj.Controls.Add(this.dateTimePicker1);
            this.groupBoxRejestruj.Controls.Add(this.textBoxRdowod);
            this.groupBoxRejestruj.Controls.Add(this.textBoxRnazwisko);
            this.groupBoxRejestruj.Controls.Add(this.textBoxRimie);
            this.groupBoxRejestruj.Controls.Add(this.textBoxRhaslo2);
            this.groupBoxRejestruj.Controls.Add(this.labelRhaslo);
            this.groupBoxRejestruj.Controls.Add(this.labelRlogin);
            this.groupBoxRejestruj.Controls.Add(this.textBoxRhaslo);
            this.groupBoxRejestruj.Controls.Add(this.textBoxRlogin);
            this.groupBoxRejestruj.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxRejestruj.Location = new System.Drawing.Point(200, 100);
            this.groupBoxRejestruj.Name = "groupBoxRejestruj";
            this.groupBoxRejestruj.Size = new System.Drawing.Size(400, 400);
            this.groupBoxRejestruj.TabIndex = 0;
            this.groupBoxRejestruj.TabStop = false;
            this.groupBoxRejestruj.Text = "Zarejestuj się";
            // 
            // buttonRanuluj
            // 
            this.buttonRanuluj.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRanuluj.Location = new System.Drawing.Point(29, 320);
            this.buttonRanuluj.Name = "buttonRanuluj";
            this.buttonRanuluj.Size = new System.Drawing.Size(98, 35);
            this.buttonRanuluj.TabIndex = 23;
            this.buttonRanuluj.Text = "Anuluj";
            this.buttonRanuluj.UseVisualStyleBackColor = true;
            this.buttonRanuluj.Click += new System.EventHandler(this.buttonRanuluj_Click);
            // 
            // buttonRzarestruj
            // 
            this.buttonRzarestruj.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRzarestruj.Location = new System.Drawing.Point(170, 320);
            this.buttonRzarestruj.Name = "buttonRzarestruj";
            this.buttonRzarestruj.Size = new System.Drawing.Size(152, 35);
            this.buttonRzarestruj.TabIndex = 22;
            this.buttonRzarestruj.Text = "Zarejestruj się";
            this.buttonRzarestruj.UseVisualStyleBackColor = true;
            this.buttonRzarestruj.Click += new System.EventHandler(this.buttonRzarestruj_Click);
            // 
            // labelRdata
            // 
            this.labelRdata.AutoSize = true;
            this.labelRdata.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRdata.Location = new System.Drawing.Point(27, 284);
            this.labelRdata.Name = "labelRdata";
            this.labelRdata.Size = new System.Drawing.Size(141, 22);
            this.labelRdata.TabIndex = 21;
            this.labelRdata.Text = "Data urodzenia:";
            // 
            // labelRdowod
            // 
            this.labelRdowod.AutoSize = true;
            this.labelRdowod.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRdowod.Location = new System.Drawing.Point(27, 250);
            this.labelRdowod.Name = "labelRdowod";
            this.labelRdowod.Size = new System.Drawing.Size(143, 22);
            this.labelRdowod.TabIndex = 20;
            this.labelRdowod.Text = "Numer dowodu:";
            // 
            // labelRnazwisko
            // 
            this.labelRnazwisko.AutoSize = true;
            this.labelRnazwisko.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRnazwisko.Location = new System.Drawing.Point(25, 216);
            this.labelRnazwisko.Name = "labelRnazwisko";
            this.labelRnazwisko.Size = new System.Drawing.Size(94, 22);
            this.labelRnazwisko.TabIndex = 19;
            this.labelRnazwisko.Text = "Nazwisko:";
            // 
            // labelRimie
            // 
            this.labelRimie.AutoSize = true;
            this.labelRimie.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRimie.Location = new System.Drawing.Point(25, 182);
            this.labelRimie.Name = "labelRimie";
            this.labelRimie.Size = new System.Drawing.Size(52, 22);
            this.labelRimie.TabIndex = 18;
            this.labelRimie.Text = "Imię:";
            // 
            // labelRhaslo2
            // 
            this.labelRhaslo2.AutoSize = true;
            this.labelRhaslo2.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRhaslo2.Location = new System.Drawing.Point(25, 154);
            this.labelRhaslo2.Name = "labelRhaslo2";
            this.labelRhaslo2.Size = new System.Drawing.Size(133, 22);
            this.labelRhaslo2.TabIndex = 17;
            this.labelRhaslo2.Text = "Powtórz hasło:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(170, 284);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(152, 28);
            this.dateTimePicker1.TabIndex = 16;
            this.dateTimePicker1.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // textBoxRdowod
            // 
            this.textBoxRdowod.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRdowod.Location = new System.Drawing.Point(170, 250);
            this.textBoxRdowod.Name = "textBoxRdowod";
            this.textBoxRdowod.Size = new System.Drawing.Size(152, 28);
            this.textBoxRdowod.TabIndex = 15;
            // 
            // textBoxRnazwisko
            // 
            this.textBoxRnazwisko.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRnazwisko.Location = new System.Drawing.Point(170, 216);
            this.textBoxRnazwisko.Name = "textBoxRnazwisko";
            this.textBoxRnazwisko.Size = new System.Drawing.Size(152, 28);
            this.textBoxRnazwisko.TabIndex = 14;
            // 
            // textBoxRimie
            // 
            this.textBoxRimie.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRimie.Location = new System.Drawing.Point(170, 182);
            this.textBoxRimie.Name = "textBoxRimie";
            this.textBoxRimie.Size = new System.Drawing.Size(152, 28);
            this.textBoxRimie.TabIndex = 3;
            // 
            // textBoxRhaslo2
            // 
            this.textBoxRhaslo2.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRhaslo2.Location = new System.Drawing.Point(170, 148);
            this.textBoxRhaslo2.Name = "textBoxRhaslo2";
            this.textBoxRhaslo2.PasswordChar = '*';
            this.textBoxRhaslo2.Size = new System.Drawing.Size(152, 28);
            this.textBoxRhaslo2.TabIndex = 2;
            // 
            // labelRhaslo
            // 
            this.labelRhaslo.AutoSize = true;
            this.labelRhaslo.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRhaslo.Location = new System.Drawing.Point(25, 114);
            this.labelRhaslo.Name = "labelRhaslo";
            this.labelRhaslo.Size = new System.Drawing.Size(63, 22);
            this.labelRhaslo.TabIndex = 13;
            this.labelRhaslo.Text = "Hasło:";
            // 
            // labelRlogin
            // 
            this.labelRlogin.AutoSize = true;
            this.labelRlogin.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRlogin.Location = new System.Drawing.Point(25, 80);
            this.labelRlogin.Name = "labelRlogin";
            this.labelRlogin.Size = new System.Drawing.Size(62, 22);
            this.labelRlogin.TabIndex = 12;
            this.labelRlogin.Text = "Login:";
            // 
            // textBoxRhaslo
            // 
            this.textBoxRhaslo.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRhaslo.Location = new System.Drawing.Point(170, 114);
            this.textBoxRhaslo.Name = "textBoxRhaslo";
            this.textBoxRhaslo.PasswordChar = '*';
            this.textBoxRhaslo.Size = new System.Drawing.Size(152, 28);
            this.textBoxRhaslo.TabIndex = 1;
            // 
            // textBoxRlogin
            // 
            this.textBoxRlogin.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxRlogin.Location = new System.Drawing.Point(170, 80);
            this.textBoxRlogin.Name = "textBoxRlogin";
            this.textBoxRlogin.Size = new System.Drawing.Size(152, 28);
            this.textBoxRlogin.TabIndex = 0;
            // 
            // panelPrzeg
            // 
            this.panelPrzeg.AutoScroll = true;
            this.panelPrzeg.Location = new System.Drawing.Point(0, 100);
            this.panelPrzeg.Name = "panelPrzeg";
            this.panelPrzeg.Size = new System.Drawing.Size(800, 500);
            this.panelPrzeg.TabIndex = 2;
            // 
            // panelUst
            // 
            this.panelUst.AutoScroll = true;
            this.panelUst.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelUst.Controls.Add(this.groupBoxUkontakt);
            this.panelUst.Controls.Add(this.groupBoxUusunkonto);
            this.panelUst.Controls.Add(this.buttonUwroc);
            this.panelUst.Controls.Add(this.panelUstAdmin);
            this.panelUst.Location = new System.Drawing.Point(1221, 455);
            this.panelUst.Name = "panelUst";
            this.panelUst.Size = new System.Drawing.Size(800, 600);
            this.panelUst.TabIndex = 3;
            // 
            // groupBoxUkontakt
            // 
            this.groupBoxUkontakt.Controls.Add(this.labelUkontakt);
            this.groupBoxUkontakt.Location = new System.Drawing.Point(464, 13);
            this.groupBoxUkontakt.Name = "groupBoxUkontakt";
            this.groupBoxUkontakt.Size = new System.Drawing.Size(280, 160);
            this.groupBoxUkontakt.TabIndex = 3;
            this.groupBoxUkontakt.TabStop = false;
            this.groupBoxUkontakt.Text = "Kontakt";
            // 
            // labelUkontakt
            // 
            this.labelUkontakt.AutoSize = true;
            this.labelUkontakt.Location = new System.Drawing.Point(11, 21);
            this.labelUkontakt.Name = "labelUkontakt";
            this.labelUkontakt.Size = new System.Drawing.Size(184, 136);
            this.labelUkontakt.TabIndex = 1;
            this.labelUkontakt.Text = "Skontaktuj sie z nami:\r\ntel. 763672812\r\ninfo@wypozyczalnias.pl\r\nUwagi w działaniu" +
    " aplikacji\r\nprosimy zgłaszać na\r\nobsluga@wypozyczalnias.pl\r\n\r\n\r\n";
            // 
            // groupBoxUusunkonto
            // 
            this.groupBoxUusunkonto.Controls.Add(this.labelUusunkonto);
            this.groupBoxUusunkonto.Controls.Add(this.buttonUusunkonto);
            this.groupBoxUusunkonto.Location = new System.Drawing.Point(65, 13);
            this.groupBoxUusunkonto.Name = "groupBoxUusunkonto";
            this.groupBoxUusunkonto.Size = new System.Drawing.Size(280, 160);
            this.groupBoxUusunkonto.TabIndex = 2;
            this.groupBoxUusunkonto.TabStop = false;
            this.groupBoxUusunkonto.Text = "Usun konto";
            // 
            // labelUusunkonto
            // 
            this.labelUusunkonto.AutoSize = true;
            this.labelUusunkonto.Location = new System.Drawing.Point(11, 21);
            this.labelUusunkonto.Name = "labelUusunkonto";
            this.labelUusunkonto.Size = new System.Drawing.Size(264, 102);
            this.labelUusunkonto.TabIndex = 1;
            this.labelUusunkonto.Text = "Kliknij aby usunąć całkowicie\r\nswoje konto wraz\r\nz przechowywanymi danymi.\r\nUwaga" +
    ", twoje dane wciąż będą dostępne\r\nna wystawionych rachunkach\r\n\r\n";
            // 
            // buttonUusunkonto
            // 
            this.buttonUusunkonto.Location = new System.Drawing.Point(199, 131);
            this.buttonUusunkonto.Name = "buttonUusunkonto";
            this.buttonUusunkonto.Size = new System.Drawing.Size(75, 23);
            this.buttonUusunkonto.TabIndex = 0;
            this.buttonUusunkonto.Text = "Usuń konto";
            this.buttonUusunkonto.UseVisualStyleBackColor = true;
            this.buttonUusunkonto.Click += new System.EventHandler(this.buttonUusunkonto_Click);
            // 
            // buttonUwroc
            // 
            this.buttonUwroc.Location = new System.Drawing.Point(695, 567);
            this.buttonUwroc.Name = "buttonUwroc";
            this.buttonUwroc.Size = new System.Drawing.Size(100, 28);
            this.buttonUwroc.TabIndex = 1;
            this.buttonUwroc.Text = "Wróc";
            this.buttonUwroc.UseVisualStyleBackColor = true;
            this.buttonUwroc.Click += new System.EventHandler(this.buttonUwroc_Click);
            // 
            // panelUstAdmin
            // 
            this.panelUstAdmin.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelUstAdmin.Controls.Add(this.groupBox1);
            this.panelUstAdmin.Controls.Add(this.groupBoxUedytujs);
            this.panelUstAdmin.Controls.Add(this.groupBoxUsunu);
            this.panelUstAdmin.Controls.Add(this.groupBoxUsuns);
            this.panelUstAdmin.Location = new System.Drawing.Point(0, 200);
            this.panelUstAdmin.Name = "panelUstAdmin";
            this.panelUstAdmin.Size = new System.Drawing.Size(800, 360);
            this.panelUstAdmin.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.idlUstawienia);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(424, 142);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 100);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edycja uzytkownika";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(40, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 22);
            this.textBox1.TabIndex = 2;
            // 
            // idlUstawienia
            // 
            this.idlUstawienia.AutoSize = true;
            this.idlUstawienia.Location = new System.Drawing.Point(11, 39);
            this.idlUstawienia.Name = "idlUstawienia";
            this.idlUstawienia.Size = new System.Drawing.Size(23, 17);
            this.idlUstawienia.TabIndex = 1;
            this.idlUstawienia.Text = "id:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Edytuj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBoxUedytujs
            // 
            this.groupBoxUedytujs.Controls.Add(this.buttonUdodaj);
            this.groupBoxUedytujs.Controls.Add(this.buttonUedytuj);
            this.groupBoxUedytujs.Controls.Add(this.textBoxUedytujid);
            this.groupBoxUedytujs.Controls.Add(this.label1);
            this.groupBoxUedytujs.Location = new System.Drawing.Point(25, 142);
            this.groupBoxUedytujs.Name = "groupBoxUedytujs";
            this.groupBoxUedytujs.Size = new System.Drawing.Size(350, 100);
            this.groupBoxUedytujs.TabIndex = 2;
            this.groupBoxUedytujs.TabStop = false;
            this.groupBoxUedytujs.Text = "Edytuj samochód";
            // 
            // buttonUdodaj
            // 
            this.buttonUdodaj.Location = new System.Drawing.Point(14, 52);
            this.buttonUdodaj.Name = "buttonUdodaj";
            this.buttonUdodaj.Size = new System.Drawing.Size(228, 24);
            this.buttonUdodaj.TabIndex = 3;
            this.buttonUdodaj.Text = "Nowy samochód";
            this.buttonUdodaj.UseVisualStyleBackColor = true;
            this.buttonUdodaj.Click += new System.EventHandler(this.buttonUdodaj_Click);
            // 
            // buttonUedytuj
            // 
            this.buttonUedytuj.Location = new System.Drawing.Point(167, 22);
            this.buttonUedytuj.Name = "buttonUedytuj";
            this.buttonUedytuj.Size = new System.Drawing.Size(75, 24);
            this.buttonUedytuj.TabIndex = 2;
            this.buttonUedytuj.Text = "Edytuj";
            this.buttonUedytuj.UseVisualStyleBackColor = true;
            this.buttonUedytuj.Click += new System.EventHandler(this.buttonUedytuj_Click);
            // 
            // textBoxUedytujid
            // 
            this.textBoxUedytujid.Location = new System.Drawing.Point(40, 22);
            this.textBoxUedytujid.Name = "textBoxUedytujid";
            this.textBoxUedytujid.Size = new System.Drawing.Size(121, 22);
            this.textBoxUedytujid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "id:";
            // 
            // groupBoxUsunu
            // 
            this.groupBoxUsunu.Controls.Add(this.buttonUsunu);
            this.groupBoxUsunu.Controls.Add(this.textBoxUsunuId);
            this.groupBoxUsunu.Controls.Add(this.labelUsunuId);
            this.groupBoxUsunu.Location = new System.Drawing.Point(424, 22);
            this.groupBoxUsunu.Name = "groupBoxUsunu";
            this.groupBoxUsunu.Size = new System.Drawing.Size(350, 100);
            this.groupBoxUsunu.TabIndex = 1;
            this.groupBoxUsunu.TabStop = false;
            this.groupBoxUsunu.Text = "Usuń użytkownika";
            // 
            // buttonUsunu
            // 
            this.buttonUsunu.Location = new System.Drawing.Point(167, 22);
            this.buttonUsunu.Name = "buttonUsunu";
            this.buttonUsunu.Size = new System.Drawing.Size(75, 24);
            this.buttonUsunu.TabIndex = 3;
            this.buttonUsunu.Text = "Usuń";
            this.buttonUsunu.UseVisualStyleBackColor = true;
            this.buttonUsunu.Click += new System.EventHandler(this.buttonUsunu_Click);
            // 
            // textBoxUsunuId
            // 
            this.textBoxUsunuId.Location = new System.Drawing.Point(40, 22);
            this.textBoxUsunuId.Name = "textBoxUsunuId";
            this.textBoxUsunuId.Size = new System.Drawing.Size(121, 22);
            this.textBoxUsunuId.TabIndex = 2;
            // 
            // labelUsunuId
            // 
            this.labelUsunuId.AutoSize = true;
            this.labelUsunuId.Location = new System.Drawing.Point(11, 25);
            this.labelUsunuId.Name = "labelUsunuId";
            this.labelUsunuId.Size = new System.Drawing.Size(23, 17);
            this.labelUsunuId.TabIndex = 1;
            this.labelUsunuId.Text = "id:";
            // 
            // groupBoxUsuns
            // 
            this.groupBoxUsuns.Controls.Add(this.buttonUsuns);
            this.groupBoxUsuns.Controls.Add(this.textBoxUsunsId);
            this.groupBoxUsuns.Controls.Add(this.labelUsunsId);
            this.groupBoxUsuns.Location = new System.Drawing.Point(25, 22);
            this.groupBoxUsuns.Name = "groupBoxUsuns";
            this.groupBoxUsuns.Size = new System.Drawing.Size(350, 100);
            this.groupBoxUsuns.TabIndex = 0;
            this.groupBoxUsuns.TabStop = false;
            this.groupBoxUsuns.Text = "Usuń samochód";
            // 
            // buttonUsuns
            // 
            this.buttonUsuns.Location = new System.Drawing.Point(167, 22);
            this.buttonUsuns.Name = "buttonUsuns";
            this.buttonUsuns.Size = new System.Drawing.Size(75, 24);
            this.buttonUsuns.TabIndex = 2;
            this.buttonUsuns.Text = "Usuń";
            this.buttonUsuns.UseVisualStyleBackColor = true;
            this.buttonUsuns.Click += new System.EventHandler(this.buttonUsuns_Click);
            // 
            // textBoxUsunsId
            // 
            this.textBoxUsunsId.Location = new System.Drawing.Point(40, 22);
            this.textBoxUsunsId.Name = "textBoxUsunsId";
            this.textBoxUsunsId.Size = new System.Drawing.Size(121, 22);
            this.textBoxUsunsId.TabIndex = 1;
            // 
            // labelUsunsId
            // 
            this.labelUsunsId.AutoSize = true;
            this.labelUsunsId.Location = new System.Drawing.Point(11, 25);
            this.labelUsunsId.Name = "labelUsunsId";
            this.labelUsunsId.Size = new System.Drawing.Size(23, 17);
            this.labelUsunsId.TabIndex = 0;
            this.labelUsunsId.Text = "id:";
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.labelMzalegasz2);
            this.panelMain.Controls.Add(this.labelMZalegasz);
            this.panelMain.Controls.Add(this.labelMwypozyczone);
            this.panelMain.Controls.Add(this.buttonMwyloguj);
            this.panelMain.Controls.Add(this.labelMuser);
            this.panelMain.Controls.Add(this.buttonMSamochody);
            this.panelMain.Controls.Add(this.buttonMust);
            this.panelMain.Controls.Add(this.panelPrzeg);
            this.panelMain.Location = new System.Drawing.Point(1309, 399);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(800, 600);
            this.panelMain.TabIndex = 4;
            // 
            // labelMzalegasz2
            // 
            this.labelMzalegasz2.AutoSize = true;
            this.labelMzalegasz2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMzalegasz2.Location = new System.Drawing.Point(281, 39);
            this.labelMzalegasz2.Name = "labelMzalegasz2";
            this.labelMzalegasz2.Size = new System.Drawing.Size(243, 34);
            this.labelMzalegasz2.TabIndex = 9;
            this.labelMzalegasz2.Text = "Sprawdź zakładkę moje samochody\r\naby oddać samochód";
            this.labelMzalegasz2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelMzalegasz2.Visible = false;
            // 
            // labelMZalegasz
            // 
            this.labelMZalegasz.AutoSize = true;
            this.labelMZalegasz.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMZalegasz.ForeColor = System.Drawing.Color.Red;
            this.labelMZalegasz.Location = new System.Drawing.Point(244, 9);
            this.labelMZalegasz.Name = "labelMZalegasz";
            this.labelMZalegasz.Size = new System.Drawing.Size(294, 22);
            this.labelMZalegasz.TabIndex = 8;
            this.labelMZalegasz.Text = "Masz wypożyczony samochód";
            this.labelMZalegasz.Visible = false;
            // 
            // labelMwypozyczone
            // 
            this.labelMwypozyczone.AutoSize = true;
            this.labelMwypozyczone.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMwypozyczone.Location = new System.Drawing.Point(13, 51);
            this.labelMwypozyczone.Name = "labelMwypozyczone";
            this.labelMwypozyczone.Size = new System.Drawing.Size(152, 22);
            this.labelMwypozyczone.TabIndex = 7;
            this.labelMwypozyczone.Text = "Wypożyczonych:";
            // 
            // buttonMwyloguj
            // 
            this.buttonMwyloguj.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonMwyloguj.Location = new System.Drawing.Point(695, 51);
            this.buttonMwyloguj.Name = "buttonMwyloguj";
            this.buttonMwyloguj.Size = new System.Drawing.Size(100, 40);
            this.buttonMwyloguj.TabIndex = 6;
            this.buttonMwyloguj.Text = "Wyloguj";
            this.buttonMwyloguj.UseVisualStyleBackColor = true;
            this.buttonMwyloguj.Click += new System.EventHandler(this.buttonMwyloguj_Click_1);
            // 
            // labelMuser
            // 
            this.labelMuser.AutoSize = true;
            this.labelMuser.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMuser.Location = new System.Drawing.Point(13, 13);
            this.labelMuser.Name = "labelMuser";
            this.labelMuser.Size = new System.Drawing.Size(57, 22);
            this.labelMuser.TabIndex = 5;
            this.labelMuser.Text = "Witaj:";
            // 
            // buttonMSamochody
            // 
            this.buttonMSamochody.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonMSamochody.Location = new System.Drawing.Point(569, 5);
            this.buttonMSamochody.Name = "buttonMSamochody";
            this.buttonMSamochody.Size = new System.Drawing.Size(180, 40);
            this.buttonMSamochody.TabIndex = 4;
            this.buttonMSamochody.Text = "Moje samochody";
            this.buttonMSamochody.UseVisualStyleBackColor = true;
            this.buttonMSamochody.Click += new System.EventHandler(this.buttonMSamochody_Click);
            // 
            // buttonMust
            // 
            this.buttonMust.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.gear;
            this.buttonMust.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonMust.Location = new System.Drawing.Point(755, 5);
            this.buttonMust.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMust.Name = "buttonMust";
            this.buttonMust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonMust.Size = new System.Drawing.Size(40, 40);
            this.buttonMust.TabIndex = 3;
            this.buttonMust.UseVisualStyleBackColor = true;
            this.buttonMust.Click += new System.EventHandler(this.buttonMust_Click);
            // 
            // panelEdytuj
            // 
            this.panelEdytuj.Controls.Add(this.groupBoxEdytuj);
            this.panelEdytuj.Location = new System.Drawing.Point(1280, 433);
            this.panelEdytuj.Name = "panelEdytuj";
            this.panelEdytuj.Size = new System.Drawing.Size(800, 600);
            this.panelEdytuj.TabIndex = 5;
            // 
            // groupBoxEdytuj
            // 
            this.groupBoxEdytuj.Controls.Add(this.numericUpDownEcena);
            this.groupBoxEdytuj.Controls.Add(this.labelEcena);
            this.groupBoxEdytuj.Controls.Add(this.labelEilosc);
            this.groupBoxEdytuj.Controls.Add(this.labelEdada);
            this.groupBoxEdytuj.Controls.Add(this.labelEid);
            this.groupBoxEdytuj.Controls.Add(this.labelEwypozyczony);
            this.groupBoxEdytuj.Controls.Add(this.labelEkonie);
            this.groupBoxEdytuj.Controls.Add(this.labelEmodel);
            this.groupBoxEdytuj.Controls.Add(this.labelEmarka);
            this.groupBoxEdytuj.Controls.Add(this.labelEvin);
            this.groupBoxEdytuj.Controls.Add(this.labelEkolor);
            this.groupBoxEdytuj.Controls.Add(this.labelEtyp);
            this.groupBoxEdytuj.Controls.Add(this.buttonEanuluj);
            this.groupBoxEdytuj.Controls.Add(this.buttonEzatwierdz);
            this.groupBoxEdytuj.Controls.Add(this.numericUpDownEilosc);
            this.groupBoxEdytuj.Controls.Add(this.dateTimePickerEdata);
            this.groupBoxEdytuj.Controls.Add(this.numericUpDownEid);
            this.groupBoxEdytuj.Controls.Add(this.checkBoxEwypozyczony);
            this.groupBoxEdytuj.Controls.Add(this.numericUpDownEkonie);
            this.groupBoxEdytuj.Controls.Add(this.textBoxEmodel);
            this.groupBoxEdytuj.Controls.Add(this.textBoxEmarka);
            this.groupBoxEdytuj.Controls.Add(this.textBoxEvin);
            this.groupBoxEdytuj.Controls.Add(this.radioButtonEciezarowy);
            this.groupBoxEdytuj.Controls.Add(this.radioButtonEosobowy);
            this.groupBoxEdytuj.Controls.Add(this.textBoxEkolor);
            this.groupBoxEdytuj.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxEdytuj.Location = new System.Drawing.Point(200, 50);
            this.groupBoxEdytuj.Name = "groupBoxEdytuj";
            this.groupBoxEdytuj.Size = new System.Drawing.Size(400, 500);
            this.groupBoxEdytuj.TabIndex = 0;
            this.groupBoxEdytuj.TabStop = false;
            this.groupBoxEdytuj.Text = "Edytuj samochod";
            // 
            // numericUpDownEcena
            // 
            this.numericUpDownEcena.DecimalPlaces = 2;
            this.numericUpDownEcena.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numericUpDownEcena.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numericUpDownEcena.Location = new System.Drawing.Point(140, 389);
            this.numericUpDownEcena.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownEcena.Name = "numericUpDownEcena";
            this.numericUpDownEcena.Size = new System.Drawing.Size(200, 27);
            this.numericUpDownEcena.TabIndex = 25;
            this.numericUpDownEcena.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // labelEcena
            // 
            this.labelEcena.AutoSize = true;
            this.labelEcena.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEcena.Location = new System.Drawing.Point(6, 391);
            this.labelEcena.Name = "labelEcena";
            this.labelEcena.Size = new System.Drawing.Size(92, 19);
            this.labelEcena.TabIndex = 24;
            this.labelEcena.Text = "Cena/dzień";
            // 
            // labelEilosc
            // 
            this.labelEilosc.AutoSize = true;
            this.labelEilosc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEilosc.Location = new System.Drawing.Point(6, 358);
            this.labelEilosc.Name = "labelEilosc";
            this.labelEilosc.Size = new System.Drawing.Size(73, 19);
            this.labelEilosc.TabIndex = 23;
            this.labelEilosc.Text = "Siedzeń:";
            // 
            // labelEdada
            // 
            this.labelEdada.AutoSize = true;
            this.labelEdada.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEdada.Location = new System.Drawing.Point(6, 329);
            this.labelEdada.Name = "labelEdada";
            this.labelEdada.Size = new System.Drawing.Size(82, 19);
            this.labelEdada.TabIndex = 22;
            this.labelEdada.Text = "Data wyp:";
            // 
            // labelEid
            // 
            this.labelEid.AutoSize = true;
            this.labelEid.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEid.Location = new System.Drawing.Point(6, 292);
            this.labelEid.Name = "labelEid";
            this.labelEid.Size = new System.Drawing.Size(94, 19);
            this.labelEid.TabIndex = 21;
            this.labelEid.Text = "Id pojazdu :";
            // 
            // labelEwypozyczony
            // 
            this.labelEwypozyczony.AutoSize = true;
            this.labelEwypozyczony.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEwypozyczony.Location = new System.Drawing.Point(6, 259);
            this.labelEwypozyczony.Name = "labelEwypozyczony";
            this.labelEwypozyczony.Size = new System.Drawing.Size(59, 19);
            this.labelEwypozyczony.TabIndex = 20;
            this.labelEwypozyczony.Text = "Status:";
            // 
            // labelEkonie
            // 
            this.labelEkonie.AutoSize = true;
            this.labelEkonie.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEkonie.Location = new System.Drawing.Point(6, 227);
            this.labelEkonie.Name = "labelEkonie";
            this.labelEkonie.Size = new System.Drawing.Size(106, 19);
            this.labelEkonie.TabIndex = 19;
            this.labelEkonie.Text = "Konie mech :";
            // 
            // labelEmodel
            // 
            this.labelEmodel.AutoSize = true;
            this.labelEmodel.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEmodel.Location = new System.Drawing.Point(6, 195);
            this.labelEmodel.Name = "labelEmodel";
            this.labelEmodel.Size = new System.Drawing.Size(57, 19);
            this.labelEmodel.TabIndex = 18;
            this.labelEmodel.Text = "Model:";
            // 
            // labelEmarka
            // 
            this.labelEmarka.AutoSize = true;
            this.labelEmarka.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEmarka.Location = new System.Drawing.Point(6, 162);
            this.labelEmarka.Name = "labelEmarka";
            this.labelEmarka.Size = new System.Drawing.Size(59, 19);
            this.labelEmarka.TabIndex = 17;
            this.labelEmarka.Text = "Marka:";
            // 
            // labelEvin
            // 
            this.labelEvin.AutoSize = true;
            this.labelEvin.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEvin.Location = new System.Drawing.Point(6, 129);
            this.labelEvin.Name = "labelEvin";
            this.labelEvin.Size = new System.Drawing.Size(41, 19);
            this.labelEvin.TabIndex = 16;
            this.labelEvin.Text = "VIN:";
            // 
            // labelEkolor
            // 
            this.labelEkolor.AutoSize = true;
            this.labelEkolor.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEkolor.Location = new System.Drawing.Point(6, 96);
            this.labelEkolor.Name = "labelEkolor";
            this.labelEkolor.Size = new System.Drawing.Size(52, 19);
            this.labelEkolor.TabIndex = 15;
            this.labelEkolor.Text = "Kolor:";
            // 
            // labelEtyp
            // 
            this.labelEtyp.AutoSize = true;
            this.labelEtyp.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEtyp.Location = new System.Drawing.Point(6, 59);
            this.labelEtyp.Name = "labelEtyp";
            this.labelEtyp.Size = new System.Drawing.Size(101, 19);
            this.labelEtyp.TabIndex = 14;
            this.labelEtyp.Text = "Typ pojazdu:";
            // 
            // buttonEanuluj
            // 
            this.buttonEanuluj.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEanuluj.Location = new System.Drawing.Point(140, 455);
            this.buttonEanuluj.Name = "buttonEanuluj";
            this.buttonEanuluj.Size = new System.Drawing.Size(100, 28);
            this.buttonEanuluj.TabIndex = 13;
            this.buttonEanuluj.Text = "Anuluj";
            this.buttonEanuluj.UseVisualStyleBackColor = true;
            this.buttonEanuluj.Click += new System.EventHandler(this.buttonEanuluj_Click);
            // 
            // buttonEzatwierdz
            // 
            this.buttonEzatwierdz.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEzatwierdz.Location = new System.Drawing.Point(242, 455);
            this.buttonEzatwierdz.Name = "buttonEzatwierdz";
            this.buttonEzatwierdz.Size = new System.Drawing.Size(100, 28);
            this.buttonEzatwierdz.TabIndex = 12;
            this.buttonEzatwierdz.Text = "Zatwierdź";
            this.buttonEzatwierdz.UseVisualStyleBackColor = true;
            this.buttonEzatwierdz.Click += new System.EventHandler(this.buttonEzatwierdz_Click);
            // 
            // numericUpDownEilosc
            // 
            this.numericUpDownEilosc.DecimalPlaces = 2;
            this.numericUpDownEilosc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numericUpDownEilosc.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numericUpDownEilosc.Location = new System.Drawing.Point(140, 356);
            this.numericUpDownEilosc.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownEilosc.Name = "numericUpDownEilosc";
            this.numericUpDownEilosc.Size = new System.Drawing.Size(200, 27);
            this.numericUpDownEilosc.TabIndex = 11;
            this.numericUpDownEilosc.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // dateTimePickerEdata
            // 
            this.dateTimePickerEdata.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePickerEdata.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerEdata.Location = new System.Drawing.Point(140, 323);
            this.dateTimePickerEdata.Name = "dateTimePickerEdata";
            this.dateTimePickerEdata.Size = new System.Drawing.Size(200, 27);
            this.dateTimePickerEdata.TabIndex = 10;
            // 
            // numericUpDownEid
            // 
            this.numericUpDownEid.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numericUpDownEid.Location = new System.Drawing.Point(140, 290);
            this.numericUpDownEid.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numericUpDownEid.Name = "numericUpDownEid";
            this.numericUpDownEid.Size = new System.Drawing.Size(200, 27);
            this.numericUpDownEid.TabIndex = 9;
            // 
            // checkBoxEwypozyczony
            // 
            this.checkBoxEwypozyczony.AutoSize = true;
            this.checkBoxEwypozyczony.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxEwypozyczony.Location = new System.Drawing.Point(140, 258);
            this.checkBoxEwypozyczony.Name = "checkBoxEwypozyczony";
            this.checkBoxEwypozyczony.Size = new System.Drawing.Size(100, 23);
            this.checkBoxEwypozyczony.TabIndex = 8;
            this.checkBoxEwypozyczony.Text = "Dostepny";
            this.checkBoxEwypozyczony.UseVisualStyleBackColor = true;
            // 
            // numericUpDownEkonie
            // 
            this.numericUpDownEkonie.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numericUpDownEkonie.Location = new System.Drawing.Point(140, 225);
            this.numericUpDownEkonie.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownEkonie.Name = "numericUpDownEkonie";
            this.numericUpDownEkonie.Size = new System.Drawing.Size(200, 27);
            this.numericUpDownEkonie.TabIndex = 7;
            // 
            // textBoxEmodel
            // 
            this.textBoxEmodel.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEmodel.Location = new System.Drawing.Point(140, 192);
            this.textBoxEmodel.Name = "textBoxEmodel";
            this.textBoxEmodel.Size = new System.Drawing.Size(200, 27);
            this.textBoxEmodel.TabIndex = 5;
            // 
            // textBoxEmarka
            // 
            this.textBoxEmarka.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEmarka.Location = new System.Drawing.Point(140, 159);
            this.textBoxEmarka.Name = "textBoxEmarka";
            this.textBoxEmarka.Size = new System.Drawing.Size(200, 27);
            this.textBoxEmarka.TabIndex = 4;
            // 
            // textBoxEvin
            // 
            this.textBoxEvin.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEvin.Location = new System.Drawing.Point(140, 126);
            this.textBoxEvin.Name = "textBoxEvin";
            this.textBoxEvin.Size = new System.Drawing.Size(200, 27);
            this.textBoxEvin.TabIndex = 3;
            // 
            // radioButtonEciezarowy
            // 
            this.radioButtonEciezarowy.AutoSize = true;
            this.radioButtonEciezarowy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButtonEciezarowy.Location = new System.Drawing.Point(242, 55);
            this.radioButtonEciezarowy.Name = "radioButtonEciezarowy";
            this.radioButtonEciezarowy.Size = new System.Drawing.Size(107, 23);
            this.radioButtonEciezarowy.TabIndex = 1;
            this.radioButtonEciezarowy.TabStop = true;
            this.radioButtonEciezarowy.Text = "Ciężarowy";
            this.radioButtonEciezarowy.UseVisualStyleBackColor = true;
            // 
            // radioButtonEosobowy
            // 
            this.radioButtonEosobowy.AutoSize = true;
            this.radioButtonEosobowy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioButtonEosobowy.Location = new System.Drawing.Point(130, 55);
            this.radioButtonEosobowy.Name = "radioButtonEosobowy";
            this.radioButtonEosobowy.Size = new System.Drawing.Size(97, 23);
            this.radioButtonEosobowy.TabIndex = 0;
            this.radioButtonEosobowy.TabStop = true;
            this.radioButtonEosobowy.Text = "Osobowy";
            this.radioButtonEosobowy.UseVisualStyleBackColor = true;
            this.radioButtonEosobowy.CheckedChanged += new System.EventHandler(this.radioButtonEosobowy_CheckedChanged);
            // 
            // textBoxEkolor
            // 
            this.textBoxEkolor.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEkolor.Location = new System.Drawing.Point(140, 93);
            this.textBoxEkolor.Name = "textBoxEkolor";
            this.textBoxEkolor.Size = new System.Drawing.Size(200, 27);
            this.textBoxEkolor.TabIndex = 2;
            // 
            // panelWypozyczone
            // 
            this.panelWypozyczone.Controls.Add(this.panelWprzeg);
            this.panelWypozyczone.Controls.Add(this.buttonWwroc);
            this.panelWypozyczone.Location = new System.Drawing.Point(1109, 562);
            this.panelWypozyczone.Name = "panelWypozyczone";
            this.panelWypozyczone.Size = new System.Drawing.Size(800, 600);
            this.panelWypozyczone.TabIndex = 6;
            // 
            // panelWprzeg
            // 
            this.panelWprzeg.AutoScroll = true;
            this.panelWprzeg.Location = new System.Drawing.Point(0, 0);
            this.panelWprzeg.Name = "panelWprzeg";
            this.panelWprzeg.Size = new System.Drawing.Size(500, 500);
            this.panelWprzeg.TabIndex = 1;
            // 
            // buttonWwroc
            // 
            this.buttonWwroc.Location = new System.Drawing.Point(706, 562);
            this.buttonWwroc.Name = "buttonWwroc";
            this.buttonWwroc.Size = new System.Drawing.Size(75, 23);
            this.buttonWwroc.TabIndex = 0;
            this.buttonWwroc.Text = "Wróć";
            this.buttonWwroc.UseVisualStyleBackColor = true;
            this.buttonWwroc.Click += new System.EventHandler(this.buttonWwroc_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 600);
            this.panel1.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePickerEdycja);
            this.groupBox2.Controls.Add(this.buttonEdAnuluj);
            this.groupBox2.Controls.Add(this.buttonEdZapisz);
            this.groupBox2.Controls.Add(this.textBoxEdN_i_S);
            this.groupBox2.Controls.Add(this.textBoxEdHas);
            this.groupBox2.Controls.Add(this.textBoxEdLog);
            this.groupBox2.Controls.Add(this.textBoxEdNazw);
            this.groupBox2.Controls.Add(this.textBoxEdImie);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(200, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 400);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Edytuj użytkownika";
            // 
            // dateTimePickerEdycja
            // 
            this.dateTimePickerEdycja.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePickerEdycja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerEdycja.Location = new System.Drawing.Point(176, 258);
            this.dateTimePickerEdycja.Name = "dateTimePickerEdycja";
            this.dateTimePickerEdycja.Size = new System.Drawing.Size(200, 27);
            this.dateTimePickerEdycja.TabIndex = 15;
            // 
            // buttonEdAnuluj
            // 
            this.buttonEdAnuluj.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEdAnuluj.Location = new System.Drawing.Point(176, 329);
            this.buttonEdAnuluj.Name = "buttonEdAnuluj";
            this.buttonEdAnuluj.Size = new System.Drawing.Size(100, 30);
            this.buttonEdAnuluj.TabIndex = 14;
            this.buttonEdAnuluj.Text = "Anuluj";
            this.buttonEdAnuluj.UseVisualStyleBackColor = true;
            this.buttonEdAnuluj.Click += new System.EventHandler(this.buttonEdAnuluj_Click);
            // 
            // buttonEdZapisz
            // 
            this.buttonEdZapisz.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEdZapisz.Location = new System.Drawing.Point(276, 329);
            this.buttonEdZapisz.Name = "buttonEdZapisz";
            this.buttonEdZapisz.Size = new System.Drawing.Size(100, 30);
            this.buttonEdZapisz.TabIndex = 13;
            this.buttonEdZapisz.Text = "Zapisz";
            this.buttonEdZapisz.UseVisualStyleBackColor = true;
            this.buttonEdZapisz.Click += new System.EventHandler(this.buttonEdZapisz_Click);
            // 
            // textBoxEdN_i_S
            // 
            this.textBoxEdN_i_S.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEdN_i_S.Location = new System.Drawing.Point(176, 224);
            this.textBoxEdN_i_S.Name = "textBoxEdN_i_S";
            this.textBoxEdN_i_S.Size = new System.Drawing.Size(200, 27);
            this.textBoxEdN_i_S.TabIndex = 10;
            // 
            // textBoxEdHas
            // 
            this.textBoxEdHas.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEdHas.Location = new System.Drawing.Point(176, 191);
            this.textBoxEdHas.Name = "textBoxEdHas";
            this.textBoxEdHas.Size = new System.Drawing.Size(200, 27);
            this.textBoxEdHas.TabIndex = 9;
            // 
            // textBoxEdLog
            // 
            this.textBoxEdLog.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEdLog.Location = new System.Drawing.Point(176, 159);
            this.textBoxEdLog.Name = "textBoxEdLog";
            this.textBoxEdLog.Size = new System.Drawing.Size(200, 27);
            this.textBoxEdLog.TabIndex = 8;
            // 
            // textBoxEdNazw
            // 
            this.textBoxEdNazw.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEdNazw.Location = new System.Drawing.Point(176, 126);
            this.textBoxEdNazw.Name = "textBoxEdNazw";
            this.textBoxEdNazw.Size = new System.Drawing.Size(200, 27);
            this.textBoxEdNazw.TabIndex = 7;
            // 
            // textBoxEdImie
            // 
            this.textBoxEdImie.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxEdImie.Location = new System.Drawing.Point(176, 93);
            this.textBoxEdImie.Name = "textBoxEdImie";
            this.textBoxEdImie.Size = new System.Drawing.Size(200, 27);
            this.textBoxEdImie.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(6, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 19);
            this.label7.TabIndex = 5;
            this.label7.Text = "data urodzenia:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(6, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 19);
            this.label6.TabIndex = 4;
            this.label6.Text = "numer i seria dowodu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(6, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "hasło:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 19);
            this.label4.TabIndex = 2;
            this.label4.Text = "login:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "nazwisko:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "imie:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelUst);
            this.Controls.Add(this.panelWypozyczone);
            this.Controls.Add(this.panelEdytuj);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelRejest);
            this.Controls.Add(this.panelLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Wypozyczalnia";
            this.panelLog.ResumeLayout(false);
            this.groupBoxLogin.ResumeLayout(false);
            this.groupBoxLogin.PerformLayout();
            this.panelRejest.ResumeLayout(false);
            this.groupBoxRejestruj.ResumeLayout(false);
            this.groupBoxRejestruj.PerformLayout();
            this.panelUst.ResumeLayout(false);
            this.groupBoxUkontakt.ResumeLayout(false);
            this.groupBoxUkontakt.PerformLayout();
            this.groupBoxUusunkonto.ResumeLayout(false);
            this.groupBoxUusunkonto.PerformLayout();
            this.panelUstAdmin.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxUedytujs.ResumeLayout(false);
            this.groupBoxUedytujs.PerformLayout();
            this.groupBoxUsunu.ResumeLayout(false);
            this.groupBoxUsunu.PerformLayout();
            this.groupBoxUsuns.ResumeLayout(false);
            this.groupBoxUsuns.PerformLayout();
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.panelEdytuj.ResumeLayout(false);
            this.groupBoxEdytuj.ResumeLayout(false);
            this.groupBoxEdytuj.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEcena)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEilosc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEkonie)).EndInit();
            this.panelWypozyczone.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.GroupBox groupBoxLogin;
        private System.Windows.Forms.Label labelLhaslo;
        private System.Windows.Forms.Label labelLlogin;
        private System.Windows.Forms.TextBox textBoxLlogin;
        private System.Windows.Forms.Button buttonLzarejestruj;
        private System.Windows.Forms.TextBox textBoxLhaslo;
        private System.Windows.Forms.Button buttonLzaloguj;
        private System.Windows.Forms.Panel panelRejest;
        private System.Windows.Forms.GroupBox groupBoxRejestruj;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBoxRdowod;
        private System.Windows.Forms.TextBox textBoxRnazwisko;
        private System.Windows.Forms.TextBox textBoxRimie;
        private System.Windows.Forms.TextBox textBoxRhaslo2;
        private System.Windows.Forms.Label labelRhaslo;
        private System.Windows.Forms.Label labelRlogin;
        private System.Windows.Forms.TextBox textBoxRhaslo;
        private System.Windows.Forms.TextBox textBoxRlogin;
        private System.Windows.Forms.Label labelRdowod;
        private System.Windows.Forms.Label labelRnazwisko;
        private System.Windows.Forms.Label labelRimie;
        private System.Windows.Forms.Label labelRhaslo2;
        private System.Windows.Forms.Button buttonRanuluj;
        private System.Windows.Forms.Button buttonRzarestruj;
        private System.Windows.Forms.Label labelRdata;
        private System.Windows.Forms.Panel panelPrzeg;
        private System.Windows.Forms.Panel panelUst;
        private System.Windows.Forms.Panel panelUstAdmin;
        private System.Windows.Forms.GroupBox groupBoxUsunu;
        private System.Windows.Forms.Button buttonUsunu;
        private System.Windows.Forms.TextBox textBoxUsunuId;
        private System.Windows.Forms.Label labelUsunuId;
        private System.Windows.Forms.GroupBox groupBoxUsuns;
        private System.Windows.Forms.Button buttonUsuns;
        private System.Windows.Forms.TextBox textBoxUsunsId;
        private System.Windows.Forms.Label labelUsunsId;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button buttonMust;
        private System.Windows.Forms.Button buttonUwroc;
        private System.Windows.Forms.GroupBox groupBoxUedytujs;
        private System.Windows.Forms.Button buttonUdodaj;
        private System.Windows.Forms.Button buttonUedytuj;
        private System.Windows.Forms.TextBox textBoxUedytujid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelEdytuj;
        private System.Windows.Forms.GroupBox groupBoxEdytuj;
        private System.Windows.Forms.CheckBox checkBoxEwypozyczony;
        private System.Windows.Forms.NumericUpDown numericUpDownEkonie;
        private System.Windows.Forms.TextBox textBoxEmodel;
        private System.Windows.Forms.TextBox textBoxEmarka;
        private System.Windows.Forms.TextBox textBoxEvin;
        private System.Windows.Forms.RadioButton radioButtonEciezarowy;
        private System.Windows.Forms.RadioButton radioButtonEosobowy;
        private System.Windows.Forms.TextBox textBoxEkolor;
        private System.Windows.Forms.Label labelEilosc;
        private System.Windows.Forms.Label labelEdada;
        private System.Windows.Forms.Label labelEid;
        private System.Windows.Forms.Label labelEwypozyczony;
        private System.Windows.Forms.Label labelEkonie;
        private System.Windows.Forms.Label labelEmodel;
        private System.Windows.Forms.Label labelEmarka;
        private System.Windows.Forms.Label labelEvin;
        private System.Windows.Forms.Label labelEkolor;
        private System.Windows.Forms.Label labelEtyp;
        private System.Windows.Forms.Button buttonEanuluj;
        private System.Windows.Forms.Button buttonEzatwierdz;
        private System.Windows.Forms.NumericUpDown numericUpDownEilosc;
        private System.Windows.Forms.DateTimePicker dateTimePickerEdata;
        private System.Windows.Forms.NumericUpDown numericUpDownEid;
        private System.Windows.Forms.Button buttonMSamochody;
        private System.Windows.Forms.Label labelMzalegasz2;
        private System.Windows.Forms.Label labelMZalegasz;
        private System.Windows.Forms.Label labelMwypozyczone;
        private System.Windows.Forms.Button buttonMwyloguj;
        private System.Windows.Forms.Label labelMuser;
        private System.Windows.Forms.Panel panelWypozyczone;
        private System.Windows.Forms.Button buttonWwroc;
        private System.Windows.Forms.Panel panelWprzeg;
        private System.Windows.Forms.NumericUpDown numericUpDownEcena;
        private System.Windows.Forms.Label labelEcena;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label idlUstawienia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxEdN_i_S;
        private System.Windows.Forms.TextBox textBoxEdHas;
        private System.Windows.Forms.TextBox textBoxEdLog;
        private System.Windows.Forms.TextBox textBoxEdNazw;
        private System.Windows.Forms.TextBox textBoxEdImie;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonEdAnuluj;
        private System.Windows.Forms.Button buttonEdZapisz;
        private System.Windows.Forms.DateTimePicker dateTimePickerEdycja;
        private System.Windows.Forms.GroupBox groupBoxUusunkonto;
        private System.Windows.Forms.Button buttonUusunkonto;
        private System.Windows.Forms.GroupBox groupBoxUkontakt;
        private System.Windows.Forms.Label labelUkontakt;
        private System.Windows.Forms.Label labelUusunkonto;
    }
}

