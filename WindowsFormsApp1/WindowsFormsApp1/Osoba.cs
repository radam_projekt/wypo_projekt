﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Osoba
    {
        int id;
        string imie;
        string nazwisko;
        string numerISeriaDowodu;
        string login;
        string haslo;
        short wiek;
        

        public Osoba(string imie, string nazwisko, string numerISeriaDowodu, string login, string haslo, short wiek, int id)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.numerISeriaDowodu = numerISeriaDowodu;
            this.login = login;
            this.haslo = haslo;
            this.wiek = wiek;
            this.id = id;
        }

        public string Imie { get => imie; set => imie = value; }
        public string Nazwisko { get => nazwisko; set => nazwisko = value; }
        public string NumerISeriaDowodu { get => numerISeriaDowodu; set => numerISeriaDowodu = value; }
        public string Login { get => login; set => login = value; }
        public string Haslo { get => haslo; set => haslo = value; }
        public short Wiek { get => wiek; set => wiek = value; }
        public int Id { get => id; set => id = value; }
    }
}
