﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WindowsFormsApp1
{
    class Garaz
    {
        private List<Osobowy> pojazdyOsobowe;
        private List<Ciezarowy> pojazdyCiezarowe;
        private string path;

        
        public Garaz(string path)
        {
            pojazdyOsobowe = new List<Osobowy>();
            pojazdyCiezarowe = new List<Ciezarowy>();
            foreach (string file in Directory.EnumerateFiles(path,"*.car"))
            {
                StreamReader sr = new StreamReader(file);
                if (sr.ReadLine() == "osobowy")
                {
                    Osobowy po = new Osobowy(sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), Convert.ToInt32(sr.ReadLine()), Convert.ToBoolean(sr.ReadLine()), Convert.ToInt32(sr.ReadLine()), Convert.ToDateTime(sr.ReadLine()), 0,0);  
                    po.IloscPasazerow = Convert.ToInt32(sr.ReadLine());
                    po.Cena = Convert.ToDouble(sr.ReadLine());
                    PojazdyOsobowe.Add(po);
                }
                else
                {
                    Ciezarowy pc = new Ciezarowy(sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), Convert.ToInt32(sr.ReadLine()), Convert.ToBoolean(sr.ReadLine()), Convert.ToInt32(sr.ReadLine()), Convert.ToDateTime(sr.ReadLine()),0,0 );
                    pc.Ladownosc = Convert.ToDouble(sr.ReadLine());
                    pc.Cena = Convert.ToDouble(sr.ReadLine());
                    pojazdyCiezarowe.Add(pc);
                }
                sr.Close();
            }
            Path = path;
        }

        public string Path { get => path; set => path = value; }

        internal List<Ciezarowy> PojazdyCiezarowe { get => pojazdyCiezarowe; set => pojazdyCiezarowe = value; }
        internal List<Osobowy> PojazdyOsobowe { get => pojazdyOsobowe; set => pojazdyOsobowe = value; }

        public void dodajPojazd(Pojazd p, bool osobowy, double l)
        {
            if (osobowy)
            {
                Osobowy po = new Osobowy(p.Kolor, p.VIN, p.Marka, p.Model, p.KonieMechaniczne, p.Wypozyczony, p.Id, Convert.ToDateTime(null),p.Cena, Convert.ToInt16(l));
                PojazdyOsobowe.Add(po);

                //File.Create(path + "/car_000" + p.Id+".car");
                StreamWriter sw = new StreamWriter(path + "/car_000" + p.Id+".car");
                sw.WriteLine("osobowy");
                sw.WriteLine(po.Kolor);
                sw.WriteLine(po.VIN);
                sw.WriteLine(po.Marka);
                sw.WriteLine(po.Model);
                sw.WriteLine(po.KonieMechaniczne.ToString());
                sw.WriteLine(po.Wypozyczony.ToString());
                sw.WriteLine(po.Id.ToString());
                sw.WriteLine(po.Datawy.ToString());
                sw.WriteLine(po.IloscPasazerow.ToString());
                sw.WriteLine(po.Cena);
                sw.Close();

            }
            else
            {
                Ciezarowy pc = new Ciezarowy(p.Kolor, p.VIN, p.Marka, p.Model, p.KonieMechaniczne, p.Wypozyczony, p.Id, Convert.ToDateTime(null),p.Cena, l);
                pojazdyCiezarowe.Add(pc);

                //File.Create(path + "/car_000" + p.Id);
                StreamWriter sw = new StreamWriter(path + "/car_000" + p.Id);
                sw.WriteLine("osobowy");
                sw.WriteLine(pc.Kolor);
                sw.WriteLine(pc.VIN);
                sw.WriteLine(pc.Marka);
                sw.WriteLine(pc.Model);
                sw.WriteLine(pc.KonieMechaniczne.ToString());
                sw.WriteLine(pc.Wypozyczony.ToString());
                sw.WriteLine(pc.Id.ToString());
                sw.WriteLine(pc.Datawy.ToString());
                sw.WriteLine(pc.Ladownosc.ToString());
                sw.WriteLine(pc.Cena.ToString());
                sw.Close();
            }
        }
        void sortujPojazdy()
        {
            PojazdyOsobowe.Sort();
            PojazdyCiezarowe.Sort();
        }
        public void aktualizuj(int id,bool osobowy)
        {    
            if(osobowy)
            {
                foreach (Osobowy po in pojazdyOsobowe)
                {
                    if(po.Id==id)
                    {
                        StreamWriter sw = new StreamWriter(path + "/car_000" + po.Id + ".car");
                        sw.WriteLine("osobowy");
                        sw.WriteLine(po.Kolor);
                        sw.WriteLine(po.VIN);
                        sw.WriteLine(po.Marka);
                        sw.WriteLine(po.Model);
                        sw.WriteLine(po.KonieMechaniczne.ToString());
                        sw.WriteLine(po.Wypozyczony.ToString());
                        sw.WriteLine(po.Id.ToString());
                        sw.WriteLine(po.Datawy.ToString());
                        sw.WriteLine(po.IloscPasazerow.ToString());
                        sw.WriteLine(po.Cena);
                        sw.Close();
                        break;
                    }
                }          
            }
            else
            {
                foreach(Ciezarowy pc in pojazdyCiezarowe)
                {
                    if(pc.Id==id)
                    {
                        StreamWriter sw = new StreamWriter(path + "/car_000" + pc.Id + ".car");
                        sw.WriteLine("ciezarowy");
                        sw.WriteLine(pc.Kolor);
                        sw.WriteLine(pc.VIN);
                        sw.WriteLine(pc.Marka);
                        sw.WriteLine(pc.Model);
                        sw.WriteLine(pc.KonieMechaniczne.ToString());
                        sw.WriteLine(pc.Wypozyczony.ToString());
                        sw.WriteLine(pc.Id.ToString());
                        sw.WriteLine(pc.Datawy.ToString());
                        sw.WriteLine(pc.Ladownosc.ToString());
                        sw.WriteLine(pc.Cena.ToString());
                        sw.Close();
                        break;
                    }
                }
            }
        }
    }

}
