#ifndef CIEZAROWY_H
#define CIEZAROWY_H
#include"pojazd.h"

class Ciezarowy : public Pojazd
{
public:
    Ciezarowy(Kolor k, QString v, QString mk, QString mo,int km,bool stan,int p_id, double lad) : Pojazd(k,v,mk,mo,km,stan,p_id),ladownosc(lad){}
    virtual double getLadownosc() const;

private:
    double ladownosc; // w [tonach]
};

#endif // CIEZAROWY_H
