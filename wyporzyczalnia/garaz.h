#ifndef GARAZ_H
#define GARAZ_H
#include<pojazd.h>
#include<vector>
#include<QFile>
#include"myexception.h"

class Garaz
{
public:
    Garaz(char* sciezka);// trzeba wczytac pojazdy w pliku
    void dodajPojazd(Pojazd p);
    void sortujPojazdy(); //sortowanie na podstawie koniMechanicznych

//private:

    std::vector<Pojazd> pojazdy;

};

#endif // GARAZ_H
