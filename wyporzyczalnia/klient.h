#ifndef KLIENT_H
#define KLIENT_H
#include "iosoba.h"
#include<memory>

class Klient : public IOsoba
{
public:
    Klient(QString i, QString n, QString nis, short w,QString log,QString h,bool u,int p_id): IOsoba(i,n,nis,w,log,h,u,p_id){}
    virtual std::vector<int> getIdWypozyczone() const;

    void setWyporzyczone(QString w); //przyjmije qstringa z id-kami samochodow
    void wypozycz(int id);
    void oddaj(int id);
    void naliczOplate();
private:

    std::vector<int> wyporzyczone; // id wyporzyczonych pojazdow
};

#endif // KLIENT_H
