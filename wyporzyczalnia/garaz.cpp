#include "garaz.h"
#include<algorithm>
#include"myexception.h"
#include<QString>
#include<QTextStream>
#include"osobowy.h"
#include"ciezarowy.h"
#include"enumclasses.h"
#include<QDir>
#include<QDebug>

Garaz::Garaz(char* sciezka) //pamietac o catchu
{

    QDir dir;
    QString path = dir.absoluteFilePath("BazaPojazdow.txt");
    qDebug(qUtf8Printable(path));
    QFile plik(path);
    if(!plik.open(QFile::ReadOnly | QFile::Text))
    {
        throw MyException();
    }

    QString wyraz[5];
    QString liczba;
    int liczby[4];
    Kolor kolor;

    auto f = [](QString k)->Kolor
    {
        if(k=="czarny")
            return Kolor::Czarny;
        if(k=="niebieski")
            return Kolor::Niebieski;
        if(k=="czerwony")
            return Kolor::Czerwony;
        else return Kolor::Inny;
    };

    QTextStream in(&plik);
    plik.readLine(); // wczytuje bezurzyteczna linie

   // Pojazd pojazd;

    while(!in.atEnd())
    {
        for(int j=0; j<5;++j)
        {
            in >> wyraz[j];
            if(in.atEnd()) break;

            qDebug(qUtf8Printable(wyraz[j]));
        }
        if(in.atEnd()) break;
        for(int j=0; j<4;++j)
        {
            in >> liczba;
            liczby[j] = liczba.toInt();
            qDebug() << liczby[j];
        }



        if(wyraz[0]=="osobowy")
        {
            qDebug() << "dodane auto";
            pojazdy.push_back(std::move(Osobowy(f(wyraz[1]),wyraz[2],wyraz[3],wyraz[4],liczby[0],liczby[1],liczby[2],liczby[3])));
        }
        else
        {
            pojazdy.push_back(std::move(Ciezarowy(f(wyraz[1]),wyraz[2],wyraz[3],wyraz[4],liczby[0],liczby[1],liczby[2],liczby[3])));
        }

    }

    plik.flush();
    plik.close();
}
void Garaz::dodajPojazd(Pojazd p)
{
    pojazdy.push_back(p);
}

void Garaz::sortujPojazdy()
{
   std::sort(pojazdy.begin(),pojazdy.end());
}
