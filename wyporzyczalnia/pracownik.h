#ifndef PRACOWNIK_H
#define PRACOWNIK_H
#include "klient.h"
#include"iosoba.h"

class Pracownik : public IOsoba
{
public:
    Pracownik(QString i, QString n, QString nis, short w,QString log,QString h,bool u,int p_id): IOsoba(i,n,nis,w,log,h,u, p_id){}
    void status();
    void edycjaUzytkownika(long id);
    void usunUzytkownika(long id);
    void dodajSamochod();
    void usunSamochod(int id);

};

#endif // PRACOWNIK_H
