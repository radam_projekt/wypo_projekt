#include "oknodlaerrorow.h"
#include "ui_oknodlaerrorow.h"

OknoDlaErrorow::OknoDlaErrorow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OknoDlaErrorow)
{
    ui->setupUi(this);
}

OknoDlaErrorow::~OknoDlaErrorow()
{
    delete ui;
}

void OknoDlaErrorow::on_pushButton_clicked()
{
    this->close();
}

