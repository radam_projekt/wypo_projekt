#include "pojazd.h"


QString Pojazd::getVIN() const
{
    return VIN;
}

QString Pojazd::getKolor() const
{
    if (kolor==Kolor::Czerwony)
        return "Czerwony";
    else if(kolor==Kolor::Czarny)
        return "Czarny";
    else if(kolor==Kolor::Niebieski)
        return "Niebieski";
    else return "Nieznany kolor";
}

QString Pojazd::getMarka() const
{
    return marka;
}

QString Pojazd::getModel() const
{
   return model;
}

bool Pojazd::getStan() const
{
    return wyporzyczony;
}

bool Pojazd::operator<(const Pojazd& rhs)
{
    if (konieMechaniczne < rhs.konieMechaniczne)
        return true;
    else return false;
}

int Pojazd::getId() const
{
    return id;
}
