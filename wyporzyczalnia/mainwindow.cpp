#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "klient.h"
#include"pracownik.h"
#include <QLineEdit>
#include <QtWidgets>
#include <QFont>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    setSciezkaDoKatalogu("/home/rskor/Dokumenty/projekt2/wypo_projekt/wyporzyczalnia/bazy/BazaPojazdow.txt");
    utworzKomponenty();
    wyswietlSamochody();
}
void MainWindow::utworzKomponenty()
{   // garaz
    garaz = new Garaz(sciezkaDoKatalogu);


    //kontener osob
    QDir dir;
    QString path = dir.absoluteFilePath("BazaUrzytkownikow.txt");
    QFile plik(path);
    if(!plik.open(QFile::ReadOnly | QFile::Text))
    {
        throw MyException();
    }

    QTextStream in(&plik);
    plik.readLine(); // wczytuje bezurzyteczna linie
    plik.readLine(); // wczytuje bezurzyteczna linie
    QString wyraz[9];

    while(!in.atEnd())
    {
        for(int i =0;i<8;++i)
        {
            in >> wyraz[i];
            if(in.atEnd()) break;
            qDebug() << wyraz[i];
        }
        if(in.atEnd()) break;

        if(wyraz[7].toInt()==0)
        {
            std::shared_ptr<Pracownik> temp = std::make_shared<Pracownik>(wyraz[3],wyraz[4],wyraz[5],wyraz[6].toInt(),wyraz[1],wyraz[2],true,wyraz[0].toInt());
            osoby.push_back(std::move(temp));

        }
        else
        {
            std::shared_ptr<Klient> temp = std::make_shared<Klient>(wyraz[3],wyraz[4],wyraz[5],wyraz[6].toInt(),wyraz[1],wyraz[2],false,wyraz[0].toInt());
            temp->setWyporzyczone(wyraz[7]);
            osoby.push_back(std::move(temp));
        }
    }
}
void MainWindow::setSciezkaDoKatalogu(char* s)
{
    sciezkaDoKatalogu=s;
}


void MainWindow::wyswietlSamochody()
{
    QFont font1("Arial",11,QFont::Normal);
    for(int i =0;i<garaz->pojazdy.size();i++)
    {
        kafelek = new QGroupBox((ui->groupBoxPrzegladaj));
        kafelek->setGeometry(0,20+i*100,500,100);
        kafelek->setFont(font1);

        lmarka = new QLabel(garaz->pojazdy[i].getMarka());
        lmarka->setParent(kafelek);
        lmarka->setGeometry(10,30,100,20);

        lmodel = new QLabel(garaz->pojazdy[i].getModel());
        lmodel->setParent(kafelek);
        lmodel->setGeometry(10,50,100,20);

        lkolor = new QLabel("Kolor: "+garaz->pojazdy[i].getKolor());
        lkolor->setParent(kafelek);
        lkolor->setGeometry(10,70,180,20);

        //lsiedzenia = new QLabel(garaz->pojazdy[i].)// nie mozna wyswietlic info o siedzeniach ładownosci. Vector w garażu to pojazd a nie cięzarowy/osobowy




        if(garaz->pojazdy[i].getStan())
        {
            pbwypozycz = new QPushButton("Wypozycz",kafelek);
            pbwypozycz->setGeometry(kafelek->width()-pbwypozycz->width()-20,30,100,40);
            //connect(pbwypozycz->clicked(),&QAction::triggered,this,[uzytkownik]{wypozycz(garaz->pojazdy[i].getId())});
        }
        ldostepny = new QLabel();
        if(garaz->pojazdy[i].getStan())ldostepny->setText("Dostępny");
        else ldostepny->setText("Niedostępny");
        ldostepny->setParent(kafelek);
        ldostepny->setGeometry(pbwypozycz->x()-100,pbwypozycz->y()+10,100,20);

    }




}

void MainWindow::wyswietlMojeSamochody()
{
    ui->labelSilosc->setText("Liczba wypozyczonych samochodow: 1");
/*
    for(int i=0;i<dynamic_cast<Klient>(*uzytkownik).getIdWypozyczone().size();i++)
    {
        ui->listWidgetSsamochody->addItem(garaz->pojazdy[dynamic_cast<Klient>(*uzytkownik).getIdWypozyczone()[i]].getMarka() + " " + garaz->pojazdy[dynamic_cast<Klient>(*uzytkownik).getIdWypozyczone()[i]].getModel());
    }
*/

}

void MainWindow::zarejestruj()
{
    QString imie,nazwisko,numer_i_seria,login,haslo1,haslo2;
    short wiek;
    imie=ui->lineEditRimie->text();
    nazwisko=ui->lineEditRnazwisko->text();
    numer_i_seria=ui->lineEditRdowod->text();
    login= ui->lineEditRlogin->text();
    haslo1=ui->lineEditRhaslo->text();
    haslo2=ui->lineEditRhaslo2->text();
    wiek = ui->dateEdit->date().year();
    bool poprawnedane=true;

    if(!(imie==nullptr || nazwisko ==nullptr || numer_i_seria ==nullptr || login==nullptr || haslo1==nullptr || haslo2==nullptr))
    {
        for(int i=0;i<osoby.size();i++)
        {
            if(osoby[i]->getLogin()==login)
            {
                poprawnedane = false;
                break;
            }
        }
        if(haslo1!=haslo2) poprawnedane = false;
        if(wiek<18)poprawnedane = false;
        if(!(ui->checkBoxRdane->isChecked())) poprawnedane = false;
    }
    else poprawnedane = false;
    if(poprawnedane)
    {
        std::shared_ptr<IOsoba> temp = std::make_shared<Klient>(imie,nazwisko,numer_i_seria,wiek,login,haslo1,false,osoby.size());
        osoby.push_back(std::move(temp));
        ui->stackedWidget->setCurrentWidget(ui->pagePrzegldaj);
    }
    else
    {
        ui->labelRblad->setText("Wprowadz poprawne dane");
    }
}

void MainWindow::zaloguj()
{
    QString login = ui->lineEditLlogin->text();
    QString haslo = ui->lineEditLhaslo->text();

    for(int i=0;i<osoby.size();i++)
    {
        if(osoby[i]->getLogin()==login && osoby[i]->getHaslo()==haslo)
        {
            uzytkownik = std::make_shared <IOsoba>(osoby[i]->getImie(),osoby[i]->getNazwisko(),osoby[i]->getNumerISeriaDowodu(),osoby[i]->getWiek(),osoby[i]->getLogin(),osoby[i]->getHaslo(),osoby[i]->getUprawnienia(),osoby[i]->getId());

            ui->stackedWidget->setCurrentWidget(ui->pagePrzegldaj);
            ui->groupBoxUadmin->setVisible(uzytkownik->getUprawnienia());
            break;
        }
        else
        {
            ui->labelLblad->setText("Wprowadz poprawne dane");

        }
    }
}


MainWindow::~MainWindow()
{
    delete garaz;
    delete ui;
}

//### SYGNAŁY/SLOTY - LOGOWANIE ###

void MainWindow::on_pushButtonLZarejestruj_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pageZarejestruj);
    //emit ui->pushButtonLZarejestruj->clicked();
}
void MainWindow::on_pushButtonZaloguj_clicked()
{

    zaloguj();
}


//### SYGNAŁY/SLOTY - REJESTRACJA ###

void MainWindow::on_pushButtonRanuluj_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pageZaloguj);
    ui->labelRblad->setText("");
}

void MainWindow::on_pushButtonRzarejestruj_clicked()
{
    zarejestruj();
}


//### SYGNAŁY/SLOTY - USTAWIENIA ###

void MainWindow::on_pushButtonUusunKonto_clicked()
{
    emit ui->pushButtonUusunKonto->clicked();
}

void MainWindow::on_pushButtonUedytujPojazd_clicked()
{
    emit ui->pushButtonUedytujPojazd->clicked();
}

void MainWindow::on_pushButtonUdodajPojazd_clicked()
{
    emit ui->pushButtonUdodajPojazd->clicked();
}

void MainWindow::on_pushButtonUusunPojazd_clicked()
{
    emit ui->pushButtonUusunPojazd->clicked();
}

void MainWindow::on_pushButtonUusunUzytkownika_clicked()
{
    emit ui->pushButtonUusunUzytkownika->clicked();
}

void MainWindow::on_pushButtonUwroc_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pagePrzegldaj);
}

//### SYGNAŁY/SLOTY - PRZEGLADAJ ###
void MainWindow::on_pushButtonPmojeSamochody_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pageMojeSamochody);
}

void MainWindow::on_pushButtonPustawienia_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pageUstawienia);
}

void MainWindow::on_pushButtonPkontakt_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pageKontakt);
}


//### SYGNAŁY/SLOTY - SAMOCHODY ###
void MainWindow::on_pushButtonSwroc_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pagePrzegldaj);
}
void MainWindow::on_listWidgetSsamochody_itemSelectionChanged()
{
    //ui->listWidgetSsamochody->currentIndex()
}




//### SYGNAŁY/SLOTY - KONTAKT  ###

void MainWindow::on_pushButtonKwroc_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->pagePrzegldaj);
}


