#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QDialog>
#include <QQueue>
#include"garaz.h"
#include"iosoba.h"
#include <QtWidgets>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setSciezkaDoKatalogu(char* s); // potrzebna do otwierania plikow
    void wyswietlSamochody();
    void wyswietlMojeSamochody();
    ~MainWindow();

private slots:
    void on_pushButtonLZarejestruj_clicked();
    void on_pushButtonRanuluj_clicked();
    void on_pushButtonRzarejestruj_clicked();
    void on_pushButtonZaloguj_clicked();
    void on_pushButtonUusunKonto_clicked();
    void on_pushButtonUedytujPojazd_clicked();
    void on_pushButtonUdodajPojazd_clicked();
    void on_pushButtonUusunPojazd_clicked();
    void on_pushButtonUusunUzytkownika_clicked();
    void on_pushButtonPmojeSamochody_clicked();
    void on_pushButtonPustawienia_clicked();
    void on_pushButtonPkontakt_clicked();

    void on_pushButtonSwroc_clicked();

    void on_pushButtonUwroc_clicked();

    void on_pushButtonKwroc_clicked();

    void on_listWidgetSsamochody_itemSelectionChanged();

    void zarejestruj();
    void zaloguj();

private:
    Ui::MainWindow *ui;
    char* sciezkaDoKatalogu;
    std::vector<std::shared_ptr<IOsoba>> osoby;
    Garaz* garaz = nullptr;
    std::shared_ptr<IOsoba> uzytkownik = nullptr;

    QGroupBox *kafelek;
    QLabel *lmarka;
    QLabel *lmodel;
    QLabel *lkolor;
    QLabel *ldostepny;
    QLabel *lkoniem;
    QLabel *lsiedzenia;
    QPushButton *pbwypozycz;
    QFont *font1;
    QLabel *labelLblad;

    void utworzKomponenty();  //do dokonczenia
};

#endif // MAINWINDOW_H
