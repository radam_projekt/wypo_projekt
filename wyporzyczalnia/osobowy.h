#ifndef OSOBOWY_H
#define OSOBOWY_H
#include"pojazd.h"

class Osobowy : public Pojazd
{
public:
    Osobowy(Kolor k, QString v, QString mk, QString mo,int km,bool stan,int p_id,short ilosc_p): Pojazd(k,v,mk,mo,km,stan,p_id){}
    virtual short getIloscPazarzerow() const;

private:

    short iloscPasarzerow;
};

#endif // OSOBOWY_H
