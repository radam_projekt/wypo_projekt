#include<iosoba.h>

QString IOsoba::getImie() const
{
    return imie;
}

QString IOsoba::getNazwisko() const
{
    return nazwisko;
}

QString IOsoba::getNumerISeriaDowodu() const
{
    return numer_i_seria_dowodu;
}

QString IOsoba::getLogin() const
{
    return login;
}

QString IOsoba::getHaslo() const
{
    return haslo;
}

short IOsoba::getWiek() const
{
    return wiek;
}

int IOsoba::getId() const
{
    return id;
}

bool IOsoba::getUprawnienia() const
{
    return uprawnienia;
}

