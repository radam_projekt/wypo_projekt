#ifndef MYEXCEPTION_H
#define MYEXCEPTION_H
#include<exception>

class MyException : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Nie udalo sie wczytac bazy danych pojazdow";
    }
};

#endif // MYEXCEPTION_H
