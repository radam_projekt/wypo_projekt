#ifndef IOSOBA_H
#define IOSOBA_H
#include<QString>

class IOsoba
{
private:
    QString imie;
    QString nazwisko;
    QString numer_i_seria_dowodu;
    short wiek;
    QString login;
    QString haslo;
    bool uprawnienia;
    int id;



public:

    IOsoba(QString i, QString n, QString nis, short w,QString log,QString h, bool u,int p_id) : imie(i), nazwisko(n), numer_i_seria_dowodu(nis), wiek(w),login(log),haslo(h), uprawnienia(u), id(p_id){}
    QString getImie() const;
    QString getNazwisko() const;
    QString getNumerISeriaDowodu() const;
    QString getLogin() const;
    QString getHaslo() const;
    short getWiek() const;
    int getId() const;
    bool getUprawnienia() const;

    virtual ~IOsoba() =default;


};

#endif // IOSOBA_H
