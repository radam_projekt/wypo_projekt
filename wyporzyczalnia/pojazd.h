#ifndef POJAZD_H
#define POJAZD_H
#include"enumclasses.h"
#include<QString>

class Pojazd
{
private:

    Kolor kolor;
    QString VIN;
    QString marka;
    QString model;
    int konieMechaniczne;
    bool wyporzyczony; //stan
    int id;


public:
   Pojazd(Kolor k, QString v, QString mk, QString mo, int km,bool s, int p_id): kolor(k), VIN(v), marka(mk), model(mo),wyporzyczony(s), konieMechaniczne(km),id(p_id){}
   QString getKolor() const; //trzeba skonwertowac enum na string
   QString getVIN() const;
   QString getMarka() const;
   QString getModel() const;
   bool getStan() const;
   int getId() const;
   bool operator<(const Pojazd& rhs);
};

#endif // POJAZD_H
